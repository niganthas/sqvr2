import React from 'react';

import { baseUrl } from 'utils';
import SecureRoute from '../components/SecureRoute';

import AuthPage from 'feachures/auth/login/page';
import RestorePage from 'feachures/auth/Restore';
import NoPassLogin from 'feachures/auth/NoPassLogin';
import Verification from 'feachures/auth/Verification';
import NewPass from 'feachures/auth/NewPass';

import RegistrationPage from 'feachures/auth/registration/Register';
import Activate from 'feachures/auth/registration/Activate';
import Contacts from 'feachures/auth/registration/Contacts';
import FromMail from 'feachures/auth/registration/FromMail';

import HomePage from '../containers/pages/Home';
import CollectionPage from '../containers/pages/Collection';
import NotFoundPage from '../containers/pages/NotFound';
import PollMainPage from '../containers/pages/PollMainPage';
import PollTemplatesPage from '../containers/pages/PollTemplatesPage';
import PollTemplatesView from '../containers/pages/PollTemplatesPage/components/ViewTemplate';
import PollResultsPage from '../containers/pages/PollResultsPage';
import CreatePollPage from '../containers/pages/CreatePollPage';
import PollPublication from '../containers/pages/PollPublication';
import PollPage from '../containers/pages/PollPage';

console.log(process.env.NODE_ENV);

export const routes = [
  {
    path: baseUrl() + '/',
    component: HomePage,
  },
  {
    path: baseUrl() + '/polls/:id/publish',
    component: PollPublication,
    secure: true,
  },
  {
    path: baseUrl() + '/polls/main',
    component: PollMainPage,
    secure: true,
  },
  {
    path: baseUrl() + '/polls/templates',
    component: PollTemplatesPage,
    secure: true,
  },
  {
    path: baseUrl() + '/polls/templates/:template',
    component: PollTemplatesView,
    secure: true,
  },
  {
    path: baseUrl() + '/polls/:id/create-poll',
    component: CreatePollPage,
    secure: true,
  },
  {
    path: baseUrl() + '/polls/:id',
    component: PollPage,
    secure: true,
  },
  {
    path: baseUrl() + '/polls/:id/results',
    component: PollResultsPage,
    secure: true,
  },
  {
    path: baseUrl() + '/collection',
    component: CollectionPage,
    secure: true,
  },
  {
    path: baseUrl() + '/auth/login',
    component: AuthPage,
    secure: false,
  },
  {
    path: baseUrl() + '/auth/restore',
    component: RestorePage,
    secure: false,
  },
  {
    path: baseUrl() + '/auth/no-pass',
    component: NoPassLogin,
    secure: false,
  },
  {
    path: baseUrl() + '/auth/verification',
    component: Verification,
    secure: false,
  },
  {
    path: baseUrl() + '/auth/new-pass',
    component: NewPass,
    secure: false,
  },
  {
    path: baseUrl() + '/registration',
    component: RegistrationPage,
    secure: false,
  },
  {
    path: baseUrl() + '/registration/callback',
    component: ({ location }) => {
      localStorage.setItem('registration_callback', location.search + location.hash);
      window.close();
      return null;
    },
    secure: false,
  },
  {
    path: baseUrl() + '/registration/activate',
    component: Activate,
    secure: false,
  },
  {
    path: baseUrl() + '/registration/contacts',
    component: Contacts,
    secure: false,
  },
  {
    path: baseUrl() + '/registration/activate/:code',
    component: FromMail,
    secure: false,
  },
  {
    path: '*',
    component: NotFoundPage,
    secure: false,
  },
];

const routeComponents = routes.map(({ path, component, secure }, key) => (
  <SecureRoute exact path={path} component={component} key={key} secure={secure} />
));

export default routeComponents;

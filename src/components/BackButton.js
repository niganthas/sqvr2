import React from 'react';
import { IconButton } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { withRouter } from 'react-router-dom';

const BackButton = ({ onClick }) => (
  <IconButton color="inherit" onClick={onClick}>
    <ArrowBackIcon />
  </IconButton>
);

export default withRouter(BackButton);

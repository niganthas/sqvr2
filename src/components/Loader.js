import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles} from '@material-ui/core';

const styles = {
  progressContainer: {
    textAlign: 'center',
    display: 'flex',
    alignContent: 'center',
    overflow: 'hidden'
  },
  progress: {
    margin: 'auto'
  },

  full: {
    height: '100vh',
  }


}

const Loader = ({ classes, loading, full }) => {
  if(loading) {
    return (
        <div className={full ? classes.progressContainer + ' ' + classes.full :classes.progressContainer}>
          <CircularProgress size={70}  thickness={1} className={classes.progress}/>
        </div>
    );
  } else return false

};

export default withStyles(styles)(Loader);

import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import gql from 'graphql-tag';
import { graphql, compose } from 'react-apollo';
import { withProps } from 'recompose';
import { get } from 'lodash';
import { baseUrl } from 'utils';
import Loader from 'components/Loader';

const checkAuthQuery = gql`
  query {
    me {
      state {
        user {
          id
          firstName
          lastName
          middleName
          avatarUrl
        }
      }
    }
  }
`;

const SecureRoute = ({ component: Component, secure, location, ...rest }) => {
  const Secure = ({ data, ...props }) => {
    if (!data.loading && !props.user && secure) {
      return <Redirect to={baseUrl() + '/auth/login'} />;
    }

    if (!data.loading && props.user && location.pathname === baseUrl() + '/auth/login') {
      return <Redirect to={baseUrl() + '/polls/main'} />;
    }

    if (!data.loading) {
      return <Component {...props} />;
    }

    return <Loader full loading={true} />;
  };

  const ComponentWithData = compose(
    graphql(checkAuthQuery),
    withProps(props => ({ user: get(props.data, 'me.state.user') }))
  )(Secure);

  return <Route {...rest} render={props => <ComponentWithData {...props} />} />;
};

export default SecureRoute;

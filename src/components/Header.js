import React from 'react';
import styled from 'styled-components';
import { media } from 'utils';
import { baseUrl } from 'utils';

const Logo = styled.div`
  width: ${p => (p.small ? '120px' : '160px')};
  height: ${p => (p.small ? '48px' : '64px')};

  ${media.desktop`display: none`};
`;

const Header = ({ small }) => (
  <header>
    <Logo small={small}>
      <img src={baseUrl() + '/img/logo.svg'} alt="Логотип Сквер" />
    </Logo>
  </header>
);

export default Header;

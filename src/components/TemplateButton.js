import React from 'react';
import { withStyles } from '@material-ui/core';
import { Text, Button } from 'components';

const styles = {
  button: {
    padding: 8,
    margin: '0 auto',
    marginBottom: 20,
    borderRadius: 3,
    width: '100%',
    display: 'flex',
    justifyContent: 'flex-start',
    textTransform: 'none',
    maxWidth: 400
  },
  title: {
    marginLeft: 8
  },
  label: {},
  desc: {
    display: 'none'
  },
  icon: {
    width: 24,
    height: 24,
    display: 'block'
  },
  '@media (min-width: 1024px)': {
    button: {
      margin: 0,
      marginRight: 20,
      marginBottom: 20,
      padding: '36px 40px',
      minHeight: 200,
      textAlign: 'center',
      width: '31.3%',
      maxWidth: 318,

      '&:nth-child(3n)': {
        marginRight: 0
      },

      '& svg': {
        fontSize: 54
      }
    },
    title: {
      fontFamily: 'Fira Sans',
      fontWeight: 500,
      fontSize: 20,
      textAlign: 'center',
      letterSpacing: '0.15px',
      margin: 0,
      color: 'rgba(0, 0, 0, 0.87)',
      marginBottom: 4
    },
    desc: {
      display: 'block'
    },
    label: {
      flexDirection: 'column',
      justifyContent: 'flex-start'
    },
    icon: {
      width: 54,
      height: 54,
      marginBottom: 16
    }
  }
};

const TemplateButton = ({ classes, ...props }) => (
  <Button
    classes={{
      root: classes.button,
      label: classes.label
    }}
    variant="contained"
    {...props}
  >
    <span className={classes.icon}>{props.children}</span>
    <Text variant="body2" component="h3" className={classes.title}>
      {props.title}
    </Text>
    <Text variant="caption" component="span" className={classes.desc}>
      {props.desc}
    </Text>
  </Button>
);

export default withStyles(styles)(TemplateButton);

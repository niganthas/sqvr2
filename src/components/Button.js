import React from 'react';
import BaseLink from './BaseLink';
import { Button } from '@material-ui/core';
import styled from 'styled-components';
import { space, width } from 'styled-system';
import { Amplitude } from '@amplitude/react-amplitude';

const StyledButton = styled(Button)`
  ${space} ${width};
`;

const ButtonTemplate = ({ AmplitudeEvent, onClick = () => {}, ...props }) => (
  <Amplitude
    eventProperties={inheritedProps => ({
      ...inheritedProps,
      scope: [...(inheritedProps.scope || {}), 'button'],
    })}
  >
    {({ instrument }) => (
      <StyledButton
        onClick={instrument(AmplitudeEvent, onClick)}
        component={props.to ? BaseLink : 'button'}
        {...props}
      >
        {props.children}
      </StyledButton>
    )}
  </Amplitude>
);

export default ButtonTemplate;

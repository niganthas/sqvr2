import React from 'react';
import { Avatar } from '@material-ui/core';
import { Text, grid } from 'components';
import styled from 'styled-components';
import { formatDate } from 'utils';

const UserIcon = styled(Avatar)`
  width: 16px;
  height: 16px;
  margin-right: 8px;
`;

const PollUserBlock = ({ src, name, date }) => (
  <grid.Flex mb="16px" alignItems="center">
    <UserIcon src={src} />
    <Text variant="caption" mr="12px">
      {name}
    </Text>
    <Text variant="caption"> {formatDate(date, 'DD.MM.YYYY')}</Text>
  </grid.Flex>
);

export default PollUserBlock;

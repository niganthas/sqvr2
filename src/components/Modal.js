import React from 'react';
import { Dialog, withStyles } from '@material-ui/core';

const styles = {
  paper: {
    position: 'relative',
    width: '90%',
    minWidth: 288,
    maxWidth: 320,
    margin: 0,
    borderRadius: 8,
    boxShadow: '0 4px 8px rgba(176, 190, 197, 0.24)'
  },
  inner: {
    position: 'absolute',
    bottom: 0,
    top: 0,
    left: 0,
    right: 0,
    opacity: 0.56,
    background:
      'linear-gradient(to top, rgba(86, 204, 242, 0.16), rgba(196, 196, 196, 0) 103px, transparent 103px, transparent 100% )'
  }
};

const Modal = ({ classes, ...props }) => {
  return (
    <Dialog
      classes={{
        paper: classes.paper
      }}
      {...props}
    >
      <div className={classes.inner} />
      {props.children}
    </Dialog>
  );
};

export default withStyles(styles)(Modal);

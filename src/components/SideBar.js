import React from 'react';
import { withStyles } from '@material-ui/core';

const styles = {
  sideBar: {
    display: 'none'
  },
  '@media(min-width: 1024px)': {
    sideBar: {
      display: 'block',
      padding: '0 32px',
      width: 204,
      flexShrink: 0,
      paddingLeft: 0
    }
  }
};

const SideBar = props => <div className={props.classes.sideBar}>{props.children}</div>;

export default withStyles(styles)(SideBar);

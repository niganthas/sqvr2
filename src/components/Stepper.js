import React, { Component } from 'react';
import { MobileStepper, Button, Typography } from '@material-ui/core';
import { KeyboardArrowLeft, KeyboardArrowRight } from '@material-ui/icons';
import styled from 'styled-components';
import * as grid from './grid';
import { baseUrl } from 'utils';

const Stepper = styled(MobileStepper)`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
`;

const Wrapper = styled.div`
  margin-top: auto;
  padding-bottom: 96px;
`;

const Steps = [
  {
    title: 'Создавайте опросы',
    desc: 'Легко создайте опрос. Быстро получите  ответы',
    source: baseUrl() + '/img/polls/boarding1.svg'
  },
  {
    title: 'Делитесь с жильцами',
    desc: 'Обсуждайте важные вопросы и проводите голосования. ',
    source: baseUrl() + '/img/polls/boarding2.svg'
  },
  {
    title: 'Получайте результаты',
    desc: 'Анализируйте результаты в режиме реального времени.',
    source: baseUrl() + '/img/polls/boarding3.svg'
  }
];

class TextMobileStepper extends Component {
  state = {
    activeStep: 0
  };
  handleNext = () => {
    this.setState(prevState => ({
      activeStep: prevState.activeStep + 1
    }));
  };

  handleBack = () => {
    this.setState(prevState => ({
      activeStep: prevState.activeStep - 1
    }));
  };
  render() {
    const { activeStep } = this.state;
    const maxSteps = Steps.length;

    return (
      <Wrapper>
        <grid.Box w="270px" mb="67px" ml="auto" mr="auto">
          <img src={Steps[activeStep].source} alt="Сквер Опросы" />
        </grid.Box>
        <Typography variant="title" align="center" gutterBottom>
          {Steps[activeStep].title}
        </Typography>
        <Typography variant="body1" align="center">
          {Steps[activeStep].desc}
        </Typography>
        <Stepper
          steps={maxSteps}
          position="static"
          activeStep={activeStep}
          nextButton={
            <Button
              size="small"
              color="primary"
              onClick={activeStep === maxSteps - 1 ? () => {} : this.handleNext}
            >
              {activeStep === maxSteps - 1 ? <a href="/">Начать</a> : 'Далее'}
              <KeyboardArrowRight />
            </Button>
          }
          backButton={
            <Button
              size="small"
              color="primary"
              onClick={this.handleBack}
              disabled={activeStep === 0}
            >
              {activeStep === 0 ? '' : <KeyboardArrowLeft />}
              {activeStep === 0 ? '' : 'Назад'}
            </Button>
          }
        />
      </Wrapper>
    );
  }
}

export default TextMobileStepper;

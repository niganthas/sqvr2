import React from 'react';
import { Badge, withStyles, IconButton } from '@material-ui/core';
import Notifications from '@material-ui/icons/Notifications';

const styles = {
  badge: {
    backgroundColor: '#FFC64C',
    borderRadius: 8
  },
  '@media (min-width: 1024px)': {
    notice: {
      margin: '0 16px'
    }
  }
};

const NoticeBadge = ({ classes, notices, children }) => (
  <IconButton
    className={classes.notice}
    color="inherit"
    onClick={() => {
      console.log('Уведомления');
    }}
  >
    <Badge
      classes={{
        badge: classes.badge
      }}
      badgeContent={notices}
      color="primary"
    >
      <Notifications />
    </Badge>
  </IconButton>
);

export default withStyles(styles)(NoticeBadge);

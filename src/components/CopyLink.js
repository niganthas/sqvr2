import React from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { TextField } from '@material-ui/core';
import { Button, grid } from 'components';
import { withState } from 'recompose';
import DoneIcon from '@material-ui/icons/Done';

const CopyLink = ({ value, copied, SetCopied }) => (
  <div>
    <TextField
      label="Ссылка на опрос"
      value={value}
      fullWidth
      inputProps={{
        readOnly: true,
      }}
    />

    <CopyToClipboard text={value} onCopy={() => SetCopied(true)}>
      <grid.Flex alignItems="center">
        <Button p="12px" ml="-12px" color="primary">
          Скопировать ссылку
        </Button>
        {copied ? <DoneIcon style={{ color: '#00DA71' }} /> : null}
      </grid.Flex>
    </CopyToClipboard>
  </div>
);

export default withState('copied', 'SetCopied', false)(CopyLink);

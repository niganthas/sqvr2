import React from 'react';
import { Text, Button, grid } from 'components';
import styled from 'styled-components';

const StyledButton = styled(Button)`
  min-height: 70px;
  padding: 12px;
  width: 140px;
  border-radius: 3px;
  margin-bottom: 16px;
  pointer-events: ${p => (p.color === 'primary' ? 'none' : 'auto')};

  & .label {
    flex-direction: column;
    justify-content: flex-start;
  }
`;

const SideButton = ({ classes, ...props }) => (
  <StyledButton
    classes={{
      label: 'label',
    }}
    {...props}
  >
    <grid.Box mb="8px">{props.icon}</grid.Box>
    <Text overline="true" color="inherit" align="center">
      {props.title}
    </Text>
  </StyledButton>
);

export default SideButton;

import React from 'react';
import styled from 'styled-components';
import Button from './Button';
import UnlockIcon from './icons/UnlockIcon';
import { space } from 'styled-system';
import Lock from '@material-ui/icons/Lock';
import { withState } from 'recompose';

const ButtonTemplate = styled(Button)`
  min-width: 40px;
  width: 40px;
  padding: 8px;
  height: 40px;
  box-shadow: 0 4px 8px rgba(176, 190, 197, 0.24);
  color: #757575;
  ${space};
`;
const LockButton = ({ locked, setLocked, ...props }) => (
  <ButtonTemplate
    {...props}
    onClick={() => {
      console.log('locked');
      setLocked(!locked);
    }}
  >
    {locked ? <Lock color="inherit" /> : <UnlockIcon />}
  </ButtonTemplate>
);

export default withState('locked', 'setLocked', false)(LockButton);

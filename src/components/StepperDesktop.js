import React from 'react';
import { MobileStepper, withStyles } from '@material-ui/core';
import { KeyboardArrowRight } from '@material-ui/icons';
import * as grid from './grid';
import { compose, withState } from 'recompose';
import { baseUrl } from 'utils';
import { Text, Button } from 'components';

const styles = {
  wrapper: {
    color: '#ffffff',
    width: 270,
    height: 470,
    textAlign: 'center',
    margin: '0 auto',
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    paddingBottom: 40,
  },
  root: {
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    padding: 0,
  },
  dot: {
    backgroundColor: '#CFD8DC',
  },
  dotActive: {
    backgroundColor: '#FFFFFF',
    boxShadow: '0 4px 8px rgba(0, 0, 0, 0.24)',
  },
  button: {
    color: 'white',
  },
};

const StepsData = [
  {
    title: 'Создавайте опросы',
    desc: 'Легко создайте опрос. Быстро получите  ответы',
    source: baseUrl() + '/img/polls/boarding1.svg',
  },
  {
    title: 'Делитесь с жильцами',
    desc: 'Обсуждайте важные вопросы и проводите голосования. ',
    source: baseUrl() + '/img/polls/boarding2.svg',
  },
  {
    title: 'Получайте результаты',
    desc: 'Анализируйте результаты в режиме реального времени.',
    source: baseUrl() + '/img/polls/boarding3.svg',
  },
];

const Stepper = props => {
  const { classes, activeStep, setActiveStep } = props;
  const maxSteps = StepsData.length;

  return (
    <div className={classes.wrapper}>
      <grid.Box h="270px" mb="20px">
        <img src={StepsData[activeStep].source} alt="Сквер Опросы" />
      </grid.Box>
      <Text variant="headline" component="h3" customcolor="#ffffff" mb="15px" mt="auto">
        {StepsData[activeStep].title}
      </Text>
      <Text variant="body2" component="p" customcolor="rgba(255, 255, 255, 0.87)" mb="30px">
        {StepsData[activeStep].desc}
      </Text>
      <MobileStepper
        classes={{
          root: classes.root,
          dot: classes.dot,
          dotActive: classes.dotActive,
        }}
        steps={maxSteps}
        position="static"
        activeStep={activeStep}
        nextButton={
          <Button
            className={classes.button}
            size="small"
            onClick={() => {
              setActiveStep(activeStep === maxSteps - 1 ? 0 : activeStep + 1);
            }}
          >
            Далее
            <KeyboardArrowRight />
          </Button>
        }
      />
    </div>
  );
};

export default compose(
  withStyles(styles),
  withState('activeStep', 'setActiveStep', 0)
)(Stepper);

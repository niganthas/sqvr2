import React from 'react';
import { withStyles } from '@material-ui/core';

const styles = {
  '@media(min-width: 1024px)': {
    main: {
      position: 'relative',
      padding: '0 24px',
      maxWidth: 1130,
      flexGrow: 2,
      margin: '0 auto',
    },
  },
  '@media(min-width: 1370px)': {
    main: {
      padding: '0 68px',
    },
  },
};

const MainContent = props => <div className={props.classes.main}>{props.children}</div>;

export default withStyles(styles)(MainContent);

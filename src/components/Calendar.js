import React from 'react';
import InfiniteCalendar from 'react-infinite-calendar';
import styled from 'styled-components';

const StyledCalendar = styled(InfiniteCalendar)`
  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);
  width: 100%;
  min-width: 288px;
  max-width: 370px;
  font-family: 'Montserrat', 'Arial', sans-serif;
  color: rgba(0, 0, 0, 0.87);
  border-radius: 8px;

  .Cal__Header__root {
    border-radius: 8px 8px 0px 0px;
  }
  .Cal__Month__rows,
  .Cal__Container__listWrapper,
  .Cal__Container__wrapper,
  .Cal__Month__partial,
  .Cal__MonthList__root {
    border-radius: 0px 0px 8px 8px;
  }
  .Cal__Header__dateWrapper.Cal__Header__day {
    font-size: 30px;
  }
  .Cal__MonthList__root {
    height: 100% !important;
  }
  .Cal__Weekdays__root {
    padding-right: 0 !important;
    color: #b0bec5 !important;
    box-shadow: 0px 4px 8px rgba(176, 190, 197, 0.24);
  }
  .Cal__Weekdays__day {
    background-color: #ffffff;
    &:first-of-type {
      border-top-left-radius: 8px;
    }
    &:last-of-type {
      border-top-right-radius: 8px;
    }
  }
  .Cal__Day__root.Cal__Day__selected .Cal__Day__selection {
    width: 48px;
  }
`;

const Calendar = ({ ...props }) => {
  return (
    <StyledCalendar
      {...props}
      locale={{
        locale: require('date-fns/locale/ru'),
        headerFormat: 'MMM D',
        weekdays: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        weekStartsOn: 1,
      }}
      height={330}
      width={'100%'}
      autoFocus={false}
      displayOptions={{
        showTodayHelper: false,
        showOverlay: false,
        shouldHeaderAnimate: false,
      }}
    />
  );
};

export default Calendar;

import Flex from './Flex';
import styled from 'styled-components';
import { media } from 'utils';

const Desktop = styled(Flex)`
  display: none;

  ${media.desktop`
    display: ${p => (p.flex ? 'flex' : 'block')};
  `};
`;

export default Desktop;

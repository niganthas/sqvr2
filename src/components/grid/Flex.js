import styled from 'styled-components';
import {
  space,
  width,
  alignItems,
  justifyContent,
  flexWrap,
  flexDirection,
  alignContent
} from 'styled-system';

const Flex = styled.div`
	${width}
	${space}
	${flexWrap}
	${alignItems}
	${justifyContent}
	${flexDirection}
	${alignContent}
	display: flex;
`;

export default Flex;

import Flex from './Flex';
import styled from 'styled-components';
import { media } from 'utils';

const Mobile = styled(Flex)`
  display: ${p => (p.flex ? 'flex' : 'block')};

  ${media.desktop`display: none;`};
`;

export default Mobile;

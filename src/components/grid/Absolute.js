import styled from 'styled-components';
import { top, right, left, bottom, width, space } from 'styled-system';

const Absolute = styled.div`
  position: absolute;
  z-index: 10;
  ${space};
  ${width};
  ${top};
  ${right};
  ${left};
  ${bottom};
`;

export default Absolute;
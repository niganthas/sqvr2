import TimePicker from 'rc-time-picker';
import styled from 'styled-components';
import 'rc-time-picker/assets/index.css';

const Picker = styled(TimePicker)`
  font-family: Montserrat, Arial, sans-serif;

  & .rc-time-picker-input {
    border: 0;
    border-bottom: 1px solid rgba(0, 0, 0, 0.42);
    padding-left: 0;
    font-size: 14px;
    font-family: Montserrat, Arial, sans-serif;
    border-radius: 0;
    background-color: transparent;

    &:focus {
      border-color: #2e87ff;
    }
  }
`;

export default Picker;

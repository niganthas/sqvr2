import React from 'react';
import {
  withStyles,
  ListSubheader,
  MenuList,
  ListItemIcon,
  ListItemText,
  Divider,
  MenuItem,
} from '@material-ui/core';
import MailOutline from '@material-ui/icons/MailOutline';
import Help from '@material-ui/icons/Help';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  header: {
    padding: 16,
    backgroundColor: '#ffffff',
  },
  number: {
    backgroundColor: '#FFC64C',
    borderRadius: 8,
    color: '#ffffff',
    fontWeight: 500,
    padding: '0 5px',
    fontSize: 14,
    lineHeight: '24px',
    minWidth: 24,
  },
});

const SubNavList = props => {
  const { classes } = props;
  return (
    <MenuList
      component="ul"
      className={classes.root}
      subheader={
        <ListSubheader className={classes.header} component="div">
          Связь
        </ListSubheader>
      }
    >
      <Divider />

      <MenuItem button>
        <ListItemIcon>
          <MailOutline />
        </ListItemIcon>
        <ListItemText inset primary="Сообщения" />
        <div className={classes.number}>15</div>
      </MenuItem>

      <MenuItem button>
        <ListItemIcon>
          <Help />
        </ListItemIcon>
        <ListItemText inset primary="Служба поддержки" />
      </MenuItem>
    </MenuList>
  );
};

export default withStyles(styles)(SubNavList);

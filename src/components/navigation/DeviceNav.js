import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import MainList from './NavList';
import SubNavList from './SubNavList';
import MenuIcon from '@material-ui/icons/Menu';

const styles = {
  list: {
    width: 264
  },
  button: {
    minWidth: 40,
    padding: 4
  }
};

class DeviceNav extends React.Component {
  state = {
    left: false
  };

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open
    });
  };

  render() {
    const { classes, user } = this.props;

    const sideList = (
      <div className={classes.list}>
        <MainList  user={user}  />
        <Divider/>
        <SubNavList />
      </div>
    );

    return (
      <div>
        <IconButton
          className={classes.button}
          onClick={this.toggleDrawer('left', true)}
          color="inherit"
        >
          <MenuIcon />
        </IconButton>
        <Drawer open={this.state.left}  onClose={this.toggleDrawer('left', false)}>
          {sideList}
        </Drawer>
      </div>
    );
  }
}

export default withStyles(styles)(DeviceNav);

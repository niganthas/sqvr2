import React from 'react';
import { grid, Text } from 'components';
import {
  withStyles,
  List,
  MenuList,
  ListSubheader,
  ListItem,
  ListItemIcon,
  ListItemText,
  Collapse,
  Divider,
  IconButton,
  ClickAwayListener,
  MenuItem,
} from '@material-ui/core';
import ArrowDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowUpIcon from '@material-ui/icons/ArrowDropUp';
import PeopleIcon from '@material-ui/icons/People';
import Tune from '@material-ui/icons/Tune';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Home from '@material-ui/icons/Home';
import Assignment from '@material-ui/icons/Assignment';
import CreditCard from '@material-ui/icons/CreditCard';
import MonetizationOn from '@material-ui/icons/MonetizationOn';
import ExitToApp from '@material-ui/icons/ExitToApp';
import AccountCircle from '@material-ui/icons/AccountCircle';
import ClipBoardIcon from 'components/icons/ClipBoardIcon';
import { compose, withState } from 'recompose';
import { baseUrl } from 'utils';
import { withRouter } from 'react-router-dom';

const styles = {
  root: {
    width: '100%',
    maxWidth: 360,
    position: 'relative',
  },
  header: {
    padding: 16,
    paddingTop: 20,
    backgroundColor: '#ffffff',
    lineHeight: '27px',
    borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
  },
  user: {
    position: 'absolute',
    top: 90,
    right: 10,
    backgroundColor: '#ffffff',
    zIndex: 10,
    width: 210,
    borderRadius: 8,
    boxShadow: '0 4px 8px rgba(0, 0, 0, 0.15)',
  },
  userButton: {
    padding: '16px 22px',
    color: 'rgba(0, 0, 0, 0.54)',
  },
  '@media (min-width: 1024px)': {
    header: {
      display: 'none',
    },
  },
};

const MainList = props => {
  const { classes, open, setOpen, active, setActive, user, history, match } = props;
  return (
    <MenuList
      className={classes.root}
      component="ul"
      subheader={
        <ListSubheader className={classes.header} component="div">
          <Text variant="title" component="h3">
            {user ? user.firstName + ' ' + user.lastName : 'Загрузка...'}
          </Text>
          <grid.Flex justifyContent="space-between" alignItems="center">
            <Text variant="body2" component="p">
              Собственник
            </Text>
            <IconButton
              onClick={() => {
                if (active === false) {
                  setActive(!active);
                }
                return;
              }}
              style={{ height: 30 }}
            >
              {active ? <ArrowUpIcon /> : <ArrowDownIcon />}
            </IconButton>
          </grid.Flex>
        </ListSubheader>
      }
    >
      {active ? (
        <ClickAwayListener
          onClickAway={e => {
            e.stopPropagation();
            setActive(!active);
          }}
        >
          <Collapse in={active} timeout="auto" unmountOnExit className={classes.user}>
            <List component="div" disablePadding>
              <ListItem button className={classes.userButton}>
                <ListItemText primary="Профиль" />
                <AccountCircle color="inherit" />
              </ListItem>
              <Divider />
              <ListItem button className={classes.userButton}>
                <ListItemText primary="Выйти" />
                <ExitToApp color="inherit" />
              </ListItem>
            </List>
          </Collapse>
        </ClickAwayListener>
      ) : null}

      <MenuItem button>
        <ListItemIcon>
          <Home />
        </ListItemIcon>
        <ListItemText inset primary="Дом" />
      </MenuItem>

      <MenuItem button onClick={() => setOpen(!open)}>
        <ListItemIcon>
          <Assignment />
        </ListItemIcon>
        <ListItemText inset primary="Объявления" />
        {open ? <ExpandLess /> : <ExpandMore />}
      </MenuItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <MenuItem button>
            <ListItemText inset primary="Жалобы" />
            <p>5</p>
          </MenuItem>
          <MenuItem button>
            <ListItemText inset primary="Объявления" />
            <p>15</p>
          </MenuItem>
        </List>
      </Collapse>

      <MenuItem button>
        <ListItemIcon>
          <PeopleIcon />
        </ListItemIcon>
        <ListItemText inset primary="Собрания" />
      </MenuItem>

      <MenuItem
        button
        selected={match.url === baseUrl() + '/polls/main'}
        onClick={() => {
          history.push(baseUrl() + '/polls/main');
        }}
      >
        <ListItemIcon>
          <ClipBoardIcon />
        </ListItemIcon>
        <ListItemText inset primary="Опросы" />
      </MenuItem>

      <MenuItem button>
        <ListItemIcon>
          <MonetizationOn />
        </ListItemIcon>
        <ListItemText inset primary="Бюджет" />
      </MenuItem>

      <MenuItem button>
        <ListItemIcon>
          <Tune />
        </ListItemIcon>
        <ListItemText inset primary="Управление" />
      </MenuItem>

      <MenuItem button>
        <ListItemIcon>
          <CreditCard />
        </ListItemIcon>
        <ListItemText inset primary="Счета" />
      </MenuItem>
    </MenuList>
  );
};

export default compose(
  withRouter,
  withStyles(styles),
  withState('open', 'setOpen', false),
  withState('active', 'setActive', false),
  withState('selected', 'setSelect', false)
)(MainList);

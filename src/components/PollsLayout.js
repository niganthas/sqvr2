import React from 'react';
import { media } from 'utils';
import { withStyles } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import MainList from './navigation/NavList';
import SubNavList from './navigation/SubNavList';
import { compose, withState } from 'recompose';
import styled from 'styled-components';
import MainAppBar from './MainAppBar';

const DesktopNav = styled.div`
  display: none;

  ${media.desktop`
    display: ${props => (props.open ? 'block' : 'none')};
    background-color: #ffffff;
    width: 260px;
    flex-shrink: 0;
    top: 68px;
    bottom: 0;
    box-shadow: 0 4px 8px rgba(176, 190, 197, 0.24);
    position: absolute;
  `};
`;
const styles = {
  root: {
    zIndex: 1,
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    minHeight: '100vh',
    minWidth: 320,
    backgroundColor: '#F7F7F7',
  },

  content: {
    display: 'flex',
    flexGrow: 2,
  },

  main: {
    flexGrow: 2,
    display: 'flex',
    flexDirection: 'column',
    padding: '32px 16px',
  },

  '@media(min-width: 1024px)': {
    main: {
      flexDirection: 'row',
      padding: '32px',
      maxWidth: 1370,
      margin: '0 auto',
    },
  },
};

const PollsLayout = props => {
  const { classes, children, open, setOpen, title, icon, main, user, onBackButtonClick } = props;
  return (
    <div className={classes.root}>
      {/* Шапка страницы */}
      <MainAppBar
        user={user}
        notices={15}
        title={title}
        open={open}
        setOpen={setOpen}
        main={main}
        icon={icon}
        onBackButtonClick={onBackButtonClick}
      />

      <main className={classes.content}>
        {/* Менюшка для десктопа */}
        <div style={{ zIndex: 10 }}>
          <DesktopNav open={open} user={user}>
            <MainList />
            <Divider />
            <SubNavList />
          </DesktopNav>
        </div>

        {/* Основной контент страницы */}
        <div className={classes.main}>{children}</div>
      </main>
    </div>
  );
};

export default compose(
  withStyles(styles),
  withState('open', 'setOpen', false)
)(PollsLayout);

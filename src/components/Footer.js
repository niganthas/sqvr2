import React from 'react';
import styled from 'styled-components';
import { Text, Button } from 'components';

const FooterRow = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Footer = () => (
  <FooterRow>
    <Text component="p" variant="body2" customcolor="rgba(0, 0, 0, 0.87)">
      Ещё нет аккаунта?
    </Text>
    <Button color="primary" mr="-16px" to="/registration">
      Регистрация
    </Button>
  </FooterRow>
);

export default Footer;

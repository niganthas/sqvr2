import React from 'react';
import styled from 'styled-components';
import LinearProgress from '@material-ui/core/LinearProgress';
import { media } from 'utils';

const StyledProgressBar = styled(LinearProgress)`
  background: rgba(38, 50, 56, 0.1);
  height: 8px;
  border-radius: 3px;
  font-size: 0;
  margin-bottom: 24px;
  width: 256px;

  ${media.desktop`
    width: 100%;
    height: 24px;
  `};
`;
const normalise = value => ((value - 0) * 100) / (100 - 0);

const ProgressBar = ({ value }) => {
  return <StyledProgressBar variant="determinate" value={normalise(value)} />;
};

export default ProgressBar;

import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';

const SignalAltIcon = props => (
  <SvgIcon {...props}>
    <path fill="none" d="M0 0h24v24H0z" />
    <path d="M17 4h3v16h-3zM5 14h3v6H5zm6-5h3v11h-3z" />
  </SvgIcon>
);

export default SignalAltIcon;

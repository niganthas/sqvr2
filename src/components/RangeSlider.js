import Slider from 'react-rangeslider';
import 'react-rangeslider/lib/index.css';
import styled from 'styled-components';

const RangeSlider = styled(Slider)`
  &.rangeslider-horizontal {
    height: 8px;

    .rangeslider__fill {
      background-color: #2e87ff;
      opacity: 0.5;
      transition: all 0.3s ease;
    }
    .rangeslider__handle {
      width: 28px;
      height: 28px;
      background-color: #2e87ff;
      border: none;
      box-shadow: 0px 4px 8px rgba(48, 79, 254, 0.24);
      transition: all 0.3s ease;

      &::after {
        display: none;
      }
    }
    .rangeslider__handle-label {
      width: 28px;
      height: 28px;
      font-size: 14px;
      color: #ffffff;
      font-family: 'Roboto';
      font-weight: 500;
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }
`;

export default RangeSlider;

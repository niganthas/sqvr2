import React from 'react';
import { withRange, Calendar } from 'react-infinite-calendar';
import { CalendarTemplate } from 'components';

const DateRangeQuestion = ({ items, onChange }) => {
  return (
    <CalendarTemplate
      Component={withRange(Calendar)}
      selected={[]}
      min={new Date(JSON.parse(items[0]))}
      max={new Date(JSON.parse(items[1]))}
      minDate={new Date(JSON.parse(items[0]))}
      maxDate={new Date(JSON.parse(items[1]))}
      onSelect={(e)=> console.log(e)}
    />
  );
};

export default DateRangeQuestion;

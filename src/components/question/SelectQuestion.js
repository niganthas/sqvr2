import React from 'react';
import { FormControlLabel, Radio, RadioGroup } from '@material-ui/core';
import { grid } from 'components';
import styled from 'styled-components';
import { withState } from 'recompose';

const Label = styled(FormControlLabel)`
  background-color: rgba(0, 0, 0, 0.04);
  margin-right: 0;
  border-radius: 3px;
  margin-bottom: 8px;
`;

const SelectQuestion = ({ options, value, setValue }) => (
  <grid.Box pl="10px">
    <RadioGroup
      value={value}
      name="options"
      onChange={e => {
        setValue(e.target.value);
      }}
    >
      {options.map((option, index) => (
        <Label
          key={index}
          value={(++index).toString()}
          control={<Radio color="primary" />}
          label={option}
        />
      ))}
    </RadioGroup>
  </grid.Box>
);

export default withState('value', 'setValue', p => p.options[0])(SelectQuestion);

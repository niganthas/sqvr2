import React from 'react';
import MultipleDateQuestion from './MultipleDateQuestion';
import IntegerRangeQuestion from './IntegerRangeQuestion';
import TimeQuestion from './TimeQuestion';
import SelectQuestion from './SelectQuestion';
import CheckboxQuestion from './CheckboxQuestion';
import { Text, grid } from 'components';

const Question = ({ index, ...props }) => (
  <grid.Box mb="40px">
    <Text variant="subheading" mb="15px">
      {index + 1}
      {'. '}
      {props.title}
    </Text>
    {props.questionType === 'date_range' && <MultipleDateQuestion {...props} />}
    {props.questionType === 'integer_range' && <IntegerRangeQuestion {...props} />}
    {props.questionType === 'time_range' && <TimeQuestion {...props} />}
    {props.questionType === 'select' && <SelectQuestion options={props.questionItems} {...props} />}
    {props.questionType === 'checkbox' && (
      <CheckboxQuestion options={props.questionItems} {...props} />
    )}
  </grid.Box>
);

export default Question;

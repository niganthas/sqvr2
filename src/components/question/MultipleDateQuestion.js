import React from 'react';
import {
  withMultipleDates,
  defaultMultipleDateInterpolation,
  Calendar,
} from 'react-infinite-calendar';
import { CalendarTemplate } from 'components';

const MultipleDateQuestion = ({ questionItems }) => (
  <CalendarTemplate
    Component={withMultipleDates(Calendar)}
    selected={[new Date(questionItems[0])]}
    interpolateSelection={defaultMultipleDateInterpolation}
    min={new Date(questionItems[0])}
    max={new Date(questionItems[1])}
    minDate={new Date(questionItems[0])}
    maxDate={new Date(questionItems[1])}
  />
);

export default MultipleDateQuestion;

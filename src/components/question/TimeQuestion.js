import React from 'react';
import { FormControlLabel, FormGroup, Checkbox } from '@material-ui/core';
import { grid } from 'components';
import styled from 'styled-components';
import { formatDate } from 'utils';

const Label = styled(FormControlLabel)`
  background-color: rgba(0, 0, 0, 0.04);
  margin-right: 0;
  border-radius: 3px;
  margin-bottom: 8px;
`;

const TimeQuestion = ({ questionItems }) => {
  return (
    <grid.Box pl="10px">
      <FormGroup>
        {questionItems.map((option, index) => (
          <Label
            key={index}
            value={option}
            control={<Checkbox value={questionItems[0]} color="primary" />}
            label={formatDate(option, 'HH:mm')}
          />
        ))}
      </FormGroup>
    </grid.Box>
  );
};

export default TimeQuestion;

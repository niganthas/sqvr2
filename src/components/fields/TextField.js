import React from 'react';
import { TextField } from '@material-ui/core';
import styled from 'styled-components';
import { space, width } from 'styled-system';

const StyledField = styled(TextField)`
  ${space} ${width}
`;

const TextFieldTemplate = ({
  id, name, label, placeholder, defaultValue, onChange, input, meta, value, error, helperText,  ...props
}) => (
  <StyledField
    {...props}
    name={input ? input.name: name}
    helperText={meta ? meta.touched ? (meta.error || meta.submitError) : undefined : helperText}
    error={meta ? (meta.error || meta.submitError) && meta.touched : error}
    inputProps={input}
    id={id}
    label={label}
    placeholder={placeholder}
    defaultValue={defaultValue}
    onChange={input ? input.onChange : onChange}
    value={input ? input.value: value}
    fullWidth
  />
);

export default TextFieldTemplate;

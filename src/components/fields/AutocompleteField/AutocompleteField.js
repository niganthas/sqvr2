import React from 'react';
import PropTypes from 'prop-types';
import Downshift from 'downshift';
import { Paper, MenuItem, withStyles, CircularProgress, InputAdornment } from '@material-ui/core';
import { fields } from 'components';
import { compose } from 'recompose';
import { withDadata } from './withDadata';

const renderSuggestion = ({ suggestion, index, itemProps, highlightedIndex, selectedItem }) => {
  const isHighlighted = highlightedIndex === index;
  const isSelected = (selectedItem || '').indexOf(suggestion.value) > -1;

  return (
    <MenuItem
      {...itemProps}
      key={suggestion.value}
      selected={isHighlighted}
      component="div"
      style={{
        fontWeight: isSelected ? 500 : 400,
      }}
    >
      {suggestion.value}
    </MenuItem>
  );
};

renderSuggestion.propTypes = {
  highlightedIndex: PropTypes.number,
  index: PropTypes.number,
  itemProps: PropTypes.object,
  selectedItem: PropTypes.string,
  suggestion: PropTypes.shape({ value: PropTypes.string }).isRequired,
};

const styles = theme => ({
  container: {
    position: 'relative',
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
  },
});

const itemToString = item => (item ? item : '');

const AutocompleteField = ({ id, placeholder, label, classes, suggestions, onSearch, input, fetching, ...props }) => (
  <Downshift
    {...input}
    onInputValueChange={inputValue => {
      input.onChange(inputValue);
      onSearch(inputValue);
    }}
    itemToString={itemToString}
    selectedItem={input.value}
  >
    {({ getInputProps, getItemProps, isOpen, inputValue, selectedItem, highlightedIndex }) => {
        console.log(getInputProps(), "getInputProps)_");
        return (
            <div className={classes.container}>
                <fields.Text
                    fullWidth
                    {...getInputProps({
                        error: props.error,
                        meta: props.meta,
                        name: input.name,
                        label,
                        placeholder,
                        InputProps: {
                            endAdornment: fetching &&
                                <InputAdornment position="end"><CircularProgress size={20}/></InputAdornment>,
                        },
                    })}
                />
                {isOpen && (
                    <Paper className={classes.paper} square>
                        {suggestions.map((suggestion, index) =>
                            renderSuggestion({
                                suggestion,
                                index,
                                itemProps: getItemProps({item: suggestion.value}),
                                highlightedIndex,
                                selectedItem,
                            })
                        )}
                    </Paper>
                )}
            </div>
        );
    }}
  </Downshift>
);

AutocompleteField.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
  withDadata,
  withStyles(styles)
)(AutocompleteField);

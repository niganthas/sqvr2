import PropTypes from 'prop-types';
import {
  compose,
  withState,
  withHandlers,
  defaultProps,
  setPropTypes,
  withRenderProps,
} from 'recompose';
import { debounce } from 'lodash';

export const withDadata = compose(
  withState('suggestions', 'setSuggestions', []),
  withState('fetching', 'changeFetchStatus', false),
  setPropTypes({
    service: PropTypes.oneOf(['address', 'fio', 'email']),
    bounds: PropTypes.arrayOf(
      PropTypes.oneOf(['region', 'area', 'city', 'settlement', 'street', 'house'])
    ),
  }),
  defaultProps({
    bounds: [],
    value: '',
    hintedValueFormer: hint => hint.value,
  }),
  withHandlers({
    fetchHint: props =>
      debounce(async text => {
        const { bounds, locations, service } = props;
        const from_bound = bounds[0] && { value: bounds[0] };
        const to_bound = bounds[0] && { value: bounds[bounds.length - 1] };
        const res = await fetch(
          `https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/${service}`,
          {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Token 8c5c2bf541b26450e0949926ebebdd2890c1333e',
            },
            body: JSON.stringify({ query: text, count: 5, from_bound, to_bound, locations }),
          }
        );

        const data = await res.json();
        console.log(data.suggestions);
        props.changeFetchStatus(false);
        props.setSuggestions(data.suggestions);
      }, 300),
  }),
  withHandlers({
    onSearch: props => text => {
      props.changeFetchStatus(true);

      props.fetchHint(text);
    },
  })
);

export const withDadataRenderProp = withRenderProps(withDadata);

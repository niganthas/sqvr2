import React from 'react';
import { AppBar, Toolbar, Avatar, IconButton } from '@material-ui/core';
import { DeviceNav, NoticeBadge, BackButton, grid, BaseLink } from 'components';
import Text from './Text';
import styled from 'styled-components';
import { baseUrl, media } from 'utils';

import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';

const Header = styled(AppBar)`
  background-color: rgba(255, 255, 255, 0.8);
  backdrop-filter: blur(8px);
  box-shadow: none;

  ${media.desktop`
      box-shadow: 0 4px 8px rgba(84, 110, 122, 0.24);
  `};
`;

const Inner = styled(Toolbar)`
  display: flex;
  justify-content: center;
  color: #757575;
  width: 100%;
  padding: 0 8px;
  box-shadow: none;

  ${media.tablet`
    padding: 0 24px;
  `};
  ${media.desktop`
    padding: 8px 24px;
  `};
`;

const Title = styled(Text)`
  margin-left: auto;
  margin-right: auto;
  ${media.desktop`margin-left: 0;`};
`;

const MainAppBar = props => {
  const { notices, title, user, setOpen, open, main, icon, onBackButtonClick } = props;
  return (
    <Header position="static">
      <Inner>
        {/* меню для мобильной версии */}
        <grid.Mobile>
          {main ? <DeviceNav user={user} /> : <BackButton onClick={onBackButtonClick} />}
        </grid.Mobile>

        {/* меню для десктопной версии */}
        <grid.Desktop>
          <IconButton
            onClick={() => {
              setOpen(!open);
            }}
            color="inherit"
          >
            <MenuIcon />
          </IconButton>
        </grid.Desktop>

        {/* логотип для десктопной версии */}
        <grid.Desktop w="45px" h="52px" mx="20px">
          <BaseLink to="/polls/main">
            <img src={baseUrl() + '/img/logo-desktop.svg'} alt="Логотип Сквер" />
          </BaseLink>
        </grid.Desktop>
        {/* Заголовок страницы */}
        <Title component="h1" variant="title">
          {title}
        </Title>

        {/* Блок с иконками десктоп*/}

        <grid.Desktop flex>
          <IconButton>
            <SearchIcon />
          </IconButton>
          <NoticeBadge notices={notices} />
          <IconButton onClick={() => console.log('Меню пользователя')}>
            <Avatar alt={user.firstName} src={user.avatarUrl} />
          </IconButton>
        </grid.Desktop>

        {/* Блок с иконками мобильная версия*/}

        <grid.Mobile>
          {main ? (
            <grid.Flex>
              <IconButton>
                <SearchIcon />
              </IconButton>
              <NoticeBadge notices={notices} />
            </grid.Flex>
          ) : (
            <grid.Box w="48px">{icon}</grid.Box>
          )}
        </grid.Mobile>
      </Inner>
    </Header>
  );
};

export default MainAppBar;

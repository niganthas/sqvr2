import React from 'react'
import { Link } from 'react-router-dom'
import { baseUrl } from "utils";

const BaseLink = (props) => {
  const { to } = props
  return <Link {...props} to={baseUrl()+to}/>
}

export default BaseLink
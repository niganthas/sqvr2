import styled, { css } from 'styled-components';
import { Typography } from '@material-ui/core';
import { space, width } from 'styled-system';

const StyledText = styled(Typography)`
  color: ${p => p.customcolor};
  ${p =>
    p.overline &&
    css`
      font-family: 'Montserrat';
      font-weight: 500;
      line-height: 12px;
      font-size: 10px;
      letter-spacing: 1.5px;
      text-transform: uppercase;
    `}
  ${space} ${width};
`;

export default StyledText;

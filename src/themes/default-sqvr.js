import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#2E87FF',
    },
    text: {
      primary: 'rgba(0, 0, 0, 0.87)',
      secondary: 'rgba(0, 0, 0, 0.6)',
      disabled: 'rgba(0, 0, 0, 0.38)',
    },
  },
  typography: {
    fontFamily: ['Montserrat', 'Arial', 'sans-serif'].join(','),

    button: {
      fontSize: 14,
      lineHeight: '17px',
      fontWeight: 500,
      letterSpacing: '0.75px',
      fontFamily: ['Fira Sans', 'Arial', 'sans-serif'].join(','),
    },
    caption: {
      fontSize: 12,
      lineHeight: '16px',
      fontWeight: 400,
      letterSpacing: '0.4px',
    },
    body2: {
      fontSize: 14,
      lineHeight: '20px',
      fontWeight: 400,
      letterSpacing: '0.25px',
      color: 'rgba(0, 0, 0, 0.6)',
    },
    body1: {
      fontSize: 16,
      lineHeight: '24px',
      fontWeight: 400,
      letterSpacing: '0.44px',
    },
    subheading: {
      fontSize: 14,
      lineHeight: '24px',
      fontWeight: 500,
      letterSpacing: '0.1px',
    },
    title: {
      fontSize: 20,
      lineHeight: '28px',
      fontWeight: 500,
      letterSpacing: '0.15px',
      fontFamily: ['Fira Sans', 'Arial', 'sans-serif'].join(','),
    },
    headline: {
      fontSize: 24,
      lineHeight: '29px',
      fontWeight: 400,
      fontFamily: ['Fira Sans', 'Arial', 'sans-serif'].join(','),
    },
    display1: {
      fontSize: 34,
      lineHeight: '41px',
      fontWeight: 400,
      letterSpacing: '0.25px',
      fontFamily: ['Fira Sans', 'Arial', 'sans-serif'].join(','),
    },
    display2: {
      fontSize: 48,
      lineHeight: '58px',
      fontWeight: 400,
      fontFamily: ['Fira Sans', 'Arial', 'sans-serif'].join(','),
    },
    display3: {
      fontSize: 60,
      lineHeight: '72px',
      fontWeight: 300,
      letterSpacing: '0.25px',
      fontFamily: ['Fira Sans', 'Arial', 'sans-serif'].join(','),
    },
    display4: {
      fontSize: 96,
      lineHeight: '115px',
      fontWeight: 300,
      letterSpacing: '-1.5px',
      fontFamily: ['Fira Sans', 'Arial', 'sans-serif'].join(','),
    },
  },
  overrides: {
    MuiFormLabel: {
      root: {
        '&$focused': {
          color: '#2E87FF',
        },
      },
    },
    MuiButton: {
      contained: {
        backgroundColor: 'white',
        boxShadow: '0px 3px 6px rgba(0, 0, 0, .1)',
        color: 'rgba(0, 0, 0, 0.87)',
        '& svg': {
          color: '#757575',
        },
      },
      containedPrimary: {
        backgroundColor: '#2E87FF',
        boxShadow: '0 4px 8px rgba(48, 79, 254, 0.24)',
        '& svg': {
          color: 'inherit',
        },
      },
      outlined: {
        borderColor: '#2E87FF',
      },
    },
    MuiInput: {
      underline: {
        '&:after': {
          borderBottom: '2px solid #2E87FF',
        },
        '&:hover:not($disabled):not($focused):not($error):before': {
          borderBottom: '1px solid #2E87FF',
        },
      },
    },
    MuiCard: {
      root: {
        boxShadow: '0px 3px 6px rgba(0, 0, 0, .1)',
      },
    },
    MuiSelect: {
      select: {
        display: 'flex',
        paddingRight: 16,
      },
    },
    MuiMenuItem: {
      root: {
        margin: '4px 8px',
        borderRadius: 4,
        '&$selected': {
          backgroundColor: 'rgba(46, 135, 255, 0.12)',
          '& svg, & span': {
            color: '#2E87FF',
          },
        },
      },
    },
  },
});

export default theme;

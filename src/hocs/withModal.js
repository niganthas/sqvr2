import React from 'react';
import { nest, compose, withState, mapProps } from 'recompose';
import { Modal as ModalContainer } from 'components';

const wrap = (...outerComponents) => wrappedComponent => nest(...outerComponents, wrappedComponent);

let outsideResolve = () => {};
export const withModal = (name, Modal) =>
  compose(
    withState('opened', 'toggleOpen', false),
    mapProps(({ opened, toggleOpen, ...rest }) => {
      return {
        [name]: {
          opened,
          open: () => {
            toggleOpen(true);
            return new Promise((res, rej) => {
              outsideResolve = res;
            });
          },
          close: result => {
            toggleOpen(false);
            outsideResolve(result);
          },
        },
        ...rest,
      };
    }),
    wrap(({ children, ...props }) => (
      <React.Fragment>
        <ModalContainer open={props[name].opened} onClose={() => props[name].close(false)}>
          {Modal(props)}
        </ModalContainer>
        {children}
      </React.Fragment>
    ))
  );

import React from 'react';
import { withModal } from '../withModal';
import { withHandlers, compose } from 'recompose';
import { graphql } from 'react-apollo';
import ModalCreate from './ModalCreate';
import gql from 'graphql-tag';
import { baseUrl } from 'utils';
import { withRouter } from 'react-router-dom';

const createPoll = gql`
  mutation {
    polls {
      createPoll(description: "", title: "") {
        id
      }
    }
  }
`;

const deletePoll = gql`
  mutation DeletePoll($pollId: Int!) {
    polls {
      deletePoll(pollId: $pollId) {
        status
      }
    }
  }
`;

const pollsQuery = gql`
  query AllPollsQuery($isAll: Boolean) {
    polls {
      listPolls(isAll: $isAll) {
        id
        status
      }
    }
  }
`;

export const withTryCreatePoll = compose(
  graphql(createPoll, {
    name: 'createPoll',
  }),
  graphql(deletePoll, {
    name: 'deletePoll',
  }),
  graphql(pollsQuery, {
    name: 'allPolls',
    options: props => ({
      fetchPolicy: 'network-only',
      variables: { isAll: true },
    }),
  }),
  withModal('modalCreate', props => <ModalCreate close={props.modalCreate.close} />),
  withRouter,
  withHandlers({
    createPoll: props => async template => {
      const { allPolls } = props;

      if (!allPolls.loading) {
        const notPublic = allPolls.polls.listPolls.find(poll => poll.status === 'draft');

        if (notPublic) {
          const closeRes = await props.modalCreate.open();
          if (!closeRes) return;

          console.log(notPublic.id);
          const resp = await props.deletePoll({
            variables: { pollId: notPublic.id },
          });

          if (resp.error) {
            return alert(resp.error.message);
          }
        }
        const newPollRequest = await props.createPoll();
        if (newPollRequest.error) {
          return alert(newPollRequest.error.message);
        }
        await allPolls.refetch();

        const pollId = newPollRequest.data.polls.createPoll.id;

        props.history.push({
          pathname: baseUrl() + '/polls/' + pollId + '/create-poll',
          state: template ? { template: template } : '',
        });
      }
    },
  })
);

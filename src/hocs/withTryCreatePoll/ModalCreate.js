import React from 'react';
import { Button, Text } from 'components';
import { DialogActions, DialogContent, DialogTitle } from '@material-ui/core';

const ModalCreate = ({ close }) => (
  <React.Fragment>
    <DialogTitle>Создать новый опрос?</DialogTitle>
    <DialogContent>
      <Text variant="body2" mb="32px">
        Создание нового опроса приведёт к удалению неопубликованного.
      </Text>
    </DialogContent>
    <DialogActions>
      <Button onClick={() => close(false)}>Вернуться</Button>
      <Button onClick={() => close(true)} color="primary">
        Создать
      </Button>
    </DialogActions>
  </React.Fragment>
);

export default ModalCreate;

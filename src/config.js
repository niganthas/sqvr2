const config = {
  URI: 'http://devsqvr.ru/api/graphql/v1',
  URI_DEV: 'http://devsqvr.ru/api/graphql/v1',
  DOMAIN: 'devsqvr.ru',
  BASE_PATH: '/n',
  API_TOKEN: '',
  GA_TOKEN: '',
  METRIKA_TOKEN: '',
  SOCIALS: {
    vk: {
      clientId: '5910052'
    }
  }
}

export default config

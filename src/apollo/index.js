import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { ApolloLink, split } from 'apollo-link';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { withClientState } from 'apollo-link-state';
import { createUploadLink } from 'apollo-upload-client';
// import { setContext } from 'apollo-link-context';
import { onError } from 'apollo-link-error';
import config from '../config';

const cache = new InMemoryCache();

const defaults = {
  modals: {
    __typename: 'modals',
    CreatePollConfirm: {
      __typename: 'CreatePollConfirmStatus',
      opened: false,
    },
  },
  networkStatus: {
    __typename: 'NetworkStatus',
    isConnected: true,
  },
};

const httpParams = {
  uri: config.URI,
  credentials: 'include',
};

const httpLink = new HttpLink(httpParams);

// const authLink = setContext((_, { headers }) => {
//   const token = localStorage.token;
//   return {
//     headers: {
//       ...headers,
//       authorization: token ? `Bearer ${token}` : '',
//     },
//   };
// });

const stateLink = withClientState({
  cache,
  resolvers: {
    Mutation: {
      updateNetworkStatus: (_, { isConnected }, { cache }) => {
        const data = {
          networkStatus: {
            __typename: 'NetworkStatus',
            isConnected,
          },
        };
        cache.writeData({ data });
        return null;
      },
    },
  },
  defaults,
});

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors)
    graphQLErrors.map(({ message, locations, path }) =>
      console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`)
    );
  if (networkError) console.log(`[Network error]: ${networkError}`);
});

const isFile = value =>
  (typeof File !== 'undefined' && value instanceof File) ||
  (typeof Blob !== 'undefined' && value instanceof Blob);

const isUpload = ({ variables }) => Object.values(variables).some(isFile);

const uploadLink = createUploadLink(httpParams);

const terminalLink = split(isUpload, uploadLink, httpLink);

const link = ApolloLink.from([stateLink, errorLink, terminalLink]);

export const client = new ApolloClient({ link, cache, connectToDevTools: true });

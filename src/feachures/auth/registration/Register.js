import React from 'react';
import { Button, grid, fields, Text } from 'components';
import { Social, Layout } from 'feachures/auth/ui';
import { graphql } from 'react-apollo';
import { compose } from 'recompose';
import gql from 'graphql-tag';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import { baseUrl } from 'utils';

import { validateRegisterUserData } from 'utils/validator';

const Form = styled.form`
  display: flex;
  flex-direction: column;
  flex-grow: 2;
`;

class Register extends React.Component {
  state = {
    data: {
      email: '',
      password: '',
      confirmationPassword: '',
    },
    errors: {},
  };

  handleChange = ({ target: { name, value } }) => {
    this.setState({
      ...this.state,
      data: {
        ...this.state.data,
        [name]: value,
      },
    });
  };

  handleSubmit = async e => {
    e.preventDefault();
    console.log(this.state);

    const errors = await validateRegisterUserData(this.state.data);
    if (!(errors === undefined)) {
      this.setState({ errors });
      return;
    }

    const response = await this.props.mutate({
      variables: {
        password: this.state.data.password,
        email: this.state.data.email,
      },
    });

    if (response.error) {
      this.setState({
        errors: {
          email: response.error.message,
        },
      });
      return;
    }

    this.props.history.push({
      pathname: baseUrl() + '/registration/activate',
      state: { email: this.state.data.email },
    });
  };

  render() {
    const {
      errors,
      data: { email, password, confirmationPassword },
    } = this.state;

    return (
      <Layout title="Регистрация">
        <grid.Box mt={['auto', 'auto', 'auto', 0]}>
          <grid.Flex justifyContent="space-between" alignItems="center" mb="24px">
            <Text pb="5px" component="h2" variant="headline">
              Регистрация
            </Text>
            <Button mr="-16px" color="primary" to="/auth/login">
              Вход
            </Button>
          </grid.Flex>
          <Form>
            <fields.Text
              error={!!errors.email}
              value={email}
              name="email"
              placeholder="Введите email"
              label="Email"
              helperText={errors.email}
              onChange={this.handleChange}
              mb="16px"
            />
            <fields.Text
              error={!!errors.password}
              type="password"
              value={password}
              name="password"
              placeholder="Введите пароль"
              label="Пароль"
              helperText={errors.password}
              onChange={this.handleChange}
              mb="16px"
            />
            <fields.Text
              error={!!errors.confirmationPassword}
              type="password"
              name="confirmationPassword"
              value={confirmationPassword}
              placeholder="Подтвердите пароль"
              label="Подтвердите пароль"
              onChange={this.handleChange}
              helperText={errors.confirmationPassword}
              mb={['36px', '36px', '36px', '80px']}
            />
            <Button
              type="submit"
              onClick={this.handleSubmit}
              variant="contained"
              color="primary"
              fullWidth
              mb="19px"
            >
              Зарегистрироваться
            </Button>
          </Form>
        </grid.Box>
        <Social />
      </Layout>
    );
  }
}

const registerMutation = gql`
  mutation($email: String!, $password: String!) {
    registration {
      stepOne(regular: { email: $email, password: $password }) {
        status
      }
    }
  }
`;

export default compose(
  withRouter,
  graphql(registerMutation)
)(Register);

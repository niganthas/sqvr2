import React from 'react';
import { Button, fields, Text } from 'components';
import { Layout } from 'feachures/auth/ui';
import styled from 'styled-components';
import { Form } from 'react-final-form';
import { graphql } from 'react-apollo';
import { compose } from 'react-apollo';
import gql from 'graphql-tag';
import { withRouter } from 'react-router-dom';
import { baseUrl } from 'utils';

const FormStyled = styled.form`
  display: flex;
  flex-direction: column;
  flex-grow: 2;
`;

class FromMail extends React.Component {
  state = {
    successCode: false,
    codeMessage: '',
  };

  componentWillMount() {
    this.props
      .activateCode({ variables: { code: this.props.match.params.code } })
      .then(response => {
        console.log(response);
        if (response.error) {
          this.setState({
            codeMessage: response.error.message,
          });
        } else {
          this.setState({
            successCode: true,
          });
        }
      });
  }

  onSubmit = async values => {
    // const response = await this.props.mutate({
    //   variables: {
    //     code: this.props.match.params.code,
    //   }
    // });

    this.props.history.push(baseUrl() + '/auth/login');
  };

  validate = values => {};

  render() {
    if (!this.state.successCode) {
      return (
        <Layout title="Регистрация">
          <Text component="h2" variant="headline" mb="32px">
            {this.state.codeMessage}
          </Text>
        </Layout>
      );
    }

    return (
      <Layout title="Регистрация">
        <Form
          onSubmit={this.onSubmit}
          validate={this.validate}
          render={({ handleSubmit, submitting, pristine, validating, submitError }) => (
            <FormStyled onSubmit={handleSubmit} mt={['56px', '56px', '56px', 0]}>
              <Text component="h2" variant="headline" mb="32px">
                Укажите контактные данные
              </Text>
              <fields.Text id="user_name" placeholder="Введите ФИО" label="ФИО" mb="16px" />
              <fields.Text id="user_address" placeholder="Введите адрес" label="Адрес" mb="16px" />
              <fields.Text
                id="user_phone"
                placeholder="Введите телефон"
                label="Телефон"
                mb="16px"
              />
              <fields.Text
                id="user_phone"
                placeholder="Введите площадь квартиры"
                label="Площадь квартиры"
                mb="16px"
              />
              <Button variant="contained" color="primary" type="submit" fullWidth mt="auto">
                Сохранить и продолжить
              </Button>
            </FormStyled>
          )}
        />
      </Layout>
    );
  }
}

const activateMutation = gql`
  mutation($code: String!) {
    registration {
      stepTwo(code: $code) {
        status
      }
    }
  }
`;

export default compose(
  withRouter,
  graphql(activateMutation, { name: 'activateCode' })
  // graphql(activateMutation, { name : 'activateCode'}),
)(FromMail);

import React from 'react';
import { Button, grid, fields, Text } from 'components';
import { Layout } from 'feachures/auth/ui';
import styled from 'styled-components';
import { Form, Field } from 'react-final-form';
import { graphql } from 'react-apollo';
import { compose } from 'recompose';
import gql from 'graphql-tag';
import { withRouter } from 'react-router-dom';
// import { baseUrl } from 'utils';

const FormStyled = styled.form`
  display: flex;
  flex-direction: column;
  flex-grow: 2;
`;

const Register = ({ location, history, mutate }) => {
  const onSubmit = async values => {
    const fio = values.fio.split(' ');

    const res = await fetch(
      `https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Token 8c5c2bf541b26450e0949926ebebdd2890c1333e',
        },
        body: JSON.stringify({ query: values.address, count: 5 }),
      }
    );
    const json = await res.json();
    if (!json.suggestions.length) {
      return { address: 'Введите адрес' };
    }

    const suggestion = json.suggestions[0].data;

    console.log(suggestion);
    if (!suggestion.city) {
      return { address: 'Не указан город' };
    }
    if (!suggestion.street) {
      return { address: 'Не указана улица' };
    }
    if (!suggestion.flat) {
      return { address: 'Не указана квартира' };
    }

    const response = await mutate({
      variables: {
        code: location.state.code,
        userData: {
          firstName: fio[0],
          lastName: fio[1],
          middleName: fio[2],
          phone: values.phone,
          ownership: values.square,
          city: suggestion.city,
          street: suggestion.street,
          house: 'dom2',
          unit: suggestion.block,
          apartment: suggestion.flat,
        },
      },
    });

    if (response.error) {
      return { code: response.error.message };
    }

    // history.push({
    //
    // });
  };

  const validate = async values => {
    const errors = {};

    if (!values.fio) {
      errors.fio = 'Введите ФИО';
    }

    if (!values.address) {
      errors.address = 'Введите адрес';
    }

    if (!values.phone) {
      errors.phone = 'Введите телефон';
    }

    if (!values.square) {
      errors.square = 'Введите площадь';
    } else if (isNaN(values.square)) {
      errors.square = 'Только числа';
    }

    return errors;
  };

  return (
    <Layout title="Регистрация">
      <Form
        onSubmit={onSubmit}
        validate={validate}
        render={({ handleSubmit, submitting, pristine, validating, submitError }) => (
          <FormStyled onSubmit={handleSubmit} mt={['56px', '56px', '56px', 0]}>
            <Text component="h2" variant="headline" mb="32px">
              Укажите контактные данные
            </Text>
            <grid.Box mb="16px">
              <Field
                name="fio"
                component={fields.Autocomplete}
                service="fio"
                placeholder="Введите ФИО"
                label="ФИО"
              />
            </grid.Box>
            <grid.Box mb="16px">
              <Field
                name="address"
                component={fields.Autocomplete}
                service="address"
                placeholder="Введите адрес"
                label="Адрес"
              />
            </grid.Box>
            <Field
              name="phone"
              component={fields.Text}
              placeholder="Введите телефон"
              label="Телефон"
              mb="16px"
            />
            <Field
              name="square"
              component={fields.Text}
              placeholder="Введите площадь квартиры"
              label="Площадь квартиры м2"
              mb="16px"
            />

            <Button variant="contained" color="primary" type="submit" fullWidth mt="auto">
              Сохранить и продолжить
            </Button>
          </FormStyled>
        )}
      />
    </Layout>
  );
};

const userDataMutation = gql`
  mutation($code: String!, $userData: [UserRegistrationInput]) {
    registration {
      stepThree(code: $code, userData: $userData) {
        user {
          id
          firstName
          lastName
          middleName
          avatarUrl
        }
      }
    }
  }
`;

export default compose(
  withRouter,
  graphql(userDataMutation)
)(Register);

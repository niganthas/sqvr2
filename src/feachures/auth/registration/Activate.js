import React from 'react';
import { Form, Field } from 'react-final-form';
import { graphql } from 'react-apollo';
import { compose } from 'recompose';
import gql from 'graphql-tag';
import { withRouter } from 'react-router-dom';
import { Button, Text, grid, fields } from 'components';
import { Layout } from 'feachures/auth/ui';
import styled from 'styled-components';
import { baseUrl } from 'utils';

const FormStyled = styled.form`
  display: flex;
  flex-direction: column;
  flex-grow: 2;
`;

const Verification = ({ location, mutate, history }) => {
  const onSubmit = async values => {
    const response = await mutate({
      variables: {
        code: values.code,
      },
    });

    if (response.error) {
      return { code: response.error.message };
    }

    history.push({
      pathname: baseUrl() + '/registration/contacts',
      state: { code: values.code },
    });
  };

  const validate = values => {
    const errors = {};
    if (!values.code) {
      errors.code = 'Введите код подтверждения';
    } else if (isNaN(values.code)) {
      errors.code = 'Только числа';
    }
    return errors;
  };

  return (
    <Layout>
      <Form
        onSubmit={onSubmit}
        validate={validate}
        render={({ handleSubmit, submitting, pristine, validating, submitError }) => (
          <FormStyled onSubmit={handleSubmit}>
            <grid.Box mt={['56px', '56px', '56px', 0]}>
              <Text component="h2" variant="headline" mb="16px">
                Пожалуйста, проверьте ваш email
              </Text>
              <Text variant="body2" component="p" mb="32px" pr="10px">
                Мы отправили вам шестизначный код на почту:{' '}
                <a style={{ fontWeight: 500, color: 'rgba(0, 0, 0, 0.87)' }} href="/">
                  {location.state.email}
                </a>. Пожалуйста введите код ниже или перейдите по ссылке в письме.
              </Text>
              <Field
                name="code"
                placeholder="Код подтверждения"
                label="Код подтверждения"
                component={fields.Text}
                mb="30px"
              />
            </grid.Box>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              mt="auto"
              mb="26px"
              disabled={submitting || pristine || validating}
            >
              Продолжить
            </Button>
          </FormStyled>
        )}
      />
      <Text variant="caption" component="p" align="center">
        Нажимая продолжить вы соглашаетесь с{' '}
        <a style={{ color: '#2E87FF' }} href="/">
          политикой конфиденциальности
        </a>{' '}
        и{' '}
        <a style={{ color: '#2E87FF' }} href="/">
          условиями пользования
        </a>
      </Text>
    </Layout>
  );
};

const activateMutation = gql`
  mutation($code: String!) {
    registration {
      stepTwo(code: $code) {
        status
      }
    }
  }
`;

export default compose(
  withRouter,
  graphql(activateMutation)
)(Verification);

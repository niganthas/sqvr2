import React from 'react';
import { Button, grid, fields, Text } from 'components';
import { Layout, Form } from 'feachures/auth/ui';

const Restore = () => (
  <Layout>
    <Form>
      <grid.Box mt={['56px', '56px', '56px', 0]}>
        <Text component="h2" variant="headline" mb="16px">
          Забыли пароль?
        </Text>
        <Text variant="body2" component="p" mb="32px">
          Укажите номер телефона или адрес электронной почты, мы отправим вам письмо с
          восстановлением доступа.
        </Text>
        <fields.Text
          id="email"
          placeholder="Email или номер тел."
          label="Email или номер тел."
          defaultValue="example@gmail.com"
        />
      </grid.Box>
      <Button variant="contained" color="primary" to="/auth/verification" mt="auto">
        Восстановить
      </Button>
    </Form>
  </Layout>
);

export default Restore;

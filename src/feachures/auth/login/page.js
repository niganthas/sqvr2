import React from 'react';
import { Button, LockButton, grid, fields, Text, BaseLink } from 'components';
import { Social, Layout } from 'feachures/auth/ui';
import styled from 'styled-components';

import { Form, Field } from 'react-final-form';
import { graphql } from 'react-apollo';
import { compose } from 'recompose';
import gql from 'graphql-tag';
import { withRouter } from 'react-router-dom';
import { redirectSubdomain } from 'utils';

const ForgotLink = styled(BaseLink)`
  font-size: 14px;
  line-height: 17px;
  font-weight: 500;
  color: rgba(0, 0, 0, 0.87);
`;

const Login = ({ history, mutate }) => {
  const onSubmit = async values => {
    const response = await mutate({
      variables: {
        login: values.login,
        password: values.password,
      },
    });

    if (response.error) {
      return { login: response.error.message };
    }

    // history.push(baseUrl() + '/polls/main');

    return redirectSubdomain('tsj');
  };

  const validate = values => {
    const errors = {};
    if (!values.login) {
      errors.login = 'Введите логин';
    }
    if (!values.password) {
      errors.password = 'Введите пароль';
    }

    return errors;
  };

  return (
    <Layout>
      <grid.Box mt={['auto', 'auto', 'auto', 0]}>
        <grid.Flex justifyContent="space-between" alignItems="center" mb="24px">
          <Text component="h2" pb="5px" variant="headline">
            Вход
          </Text>
          <Button mr="-16px" color="primary" to="/registration">
            Регистрация
          </Button>
        </grid.Flex>
        <Form
          onSubmit={onSubmit}
          validate={validate}
          render={({ handleSubmit, submitting, pristine, validating, submitError }) => (
            <form onSubmit={handleSubmit}>
              {/* <InputWithDadataRenderProp>{() => 123}</InputWithDadataRenderProp> */}
              <grid.Box mb="16px">
                <Field
                  name="login"
                  placeholder="Введите email или номер тел."
                  label="Email или номер тел."
                  component={fields.Autocomplete}
                  service="email"
                />
              </grid.Box>
              <Field
                name="password"
                placeholder="Введите пароль"
                type="password"
                label="Пароль"
                mb="24px"
                component={fields.Text}
              />
              <grid.Flex justifyContent="flex-end" mb={[40, 40, 40, 100]}>
                <ForgotLink to="/auth/restore">Забыли пароль?</ForgotLink>
              </grid.Flex>
              <grid.Flex justifyContent="flex-end" mb="24px">
                <Button
                  variant="contained"
                  type="submit"
                  color="primary"
                  disabled={pristine || validating || submitting}
                  fullWidth
                >
                  Войти
                </Button>
                <LockButton ml="16px" />
              </grid.Flex>
            </form>
          )}
        />
      </grid.Box>
      <Social />
    </Layout>
  );
};

const loginMutation = gql`
  mutation($login: String!, $password: String!) {
    me {
      userLogin(regular: { username: $login, password: $password }) {
        id
        firstName
        api_key
      }
    }
  }
`;

export default compose(
  withRouter,
  graphql(loginMutation)
)(Login);

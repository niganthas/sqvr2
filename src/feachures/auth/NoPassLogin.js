import React from 'react';
import { Button, grid, fields, Text } from 'components';
import { Layout, Form } from 'feachures/auth/ui';

const NoPassLogin = () => (
  <Layout>
    <Form>
      <grid.Box mt={['56px', '56px', '56px', 0]}>
        <Text component="h2" variant="headline" mb="102px">
          Желаете войти без пароля?
        </Text>
        <fields.Text id="email" placeholder="Email или номер тел." label="Email или номер тел." />
      </grid.Box>
      <Button variant="contained" color="primary" to="/auth/verification" mt="auto">
        Получить Magiclink
      </Button>
    </Form>
  </Layout>
);

export default NoPassLogin;

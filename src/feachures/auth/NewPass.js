import React from 'react';
import { Button, fields, grid, Text } from 'components';
import { Form, Layout } from 'feachures/auth/ui';

const NewPass = () => (
  <Layout title="Новый пароль">
    <Form>
      <grid.Box mt={['56px', '56px', '56px', 0]}>
        <Text component="h2" variant="headline" mb="16px">
          Введите новый пароль
        </Text>
        <fields.Text id="password" placeholder="Введите новый пароль" label="Пароль" mb="16px" />
        <fields.Text
          id="password_repeat"
          placeholder="Подтвердите пароль"
          label="Подтвердите пароль"
        />
      </grid.Box>
      <Button variant="contained" color="primary" to="/" mt="auto">
        сохранить и продолжить
      </Button>
    </Form>
  </Layout>
);

export default NewPass;

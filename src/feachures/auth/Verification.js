import React from 'react';
import { Button, grid, fields, Text } from 'components';
import { Layout, Form } from 'feachures/auth/ui';

const UserData = {
  type: 'phone',
  link: '8 (999) 898-98-98',
};

const Verification = () => (
  <Layout>
    <Form>
      <grid.Box mt={['56px', '56px', '56px', 0]}>
        <Text component="h2" variant="headline" mb="16px">
          Пожалуйста, проверьте ваш {UserData.type === 'email' ? 'email' : 'телефон'}
        </Text>
        <Text variant="body2" component="p" mb="32px" pr="10px">
          Мы отправили вам шестизначный код на {UserData.type === 'email' ? 'почту' : 'номер'}:{' '}
          <a style={{ fontWeight: 500, color: 'rgba(0, 0, 0, 0.87)' }} href="/">
            {UserData.link}
          </a>
          . Пожалуйста введите код ниже или перейдите по ссылке в{' '}
          {UserData.type === 'email' ? 'письме' : 'сообщении'}.
        </Text>
        <fields.Text
          id="verification_code"
          placeholder="Код подтверждения"
          label="Код подтверждения"
        />
      </grid.Box>
      <Button variant="contained" color="primary" to="/auth/new-pass" mt="auto">
        Продолжить
      </Button>
    </Form>
  </Layout>
);

export default Verification;

import styled from 'styled-components';
import { media } from 'utils';

export const Wrapper = styled.div`
  ${media.desktop`
    margin-top: auto;
  `};
`;

export const TextBlock = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 8px;

  &::before,
  &::after {
    content: '';
    width: 20px;
    height: 2px;
    flex-grow: 2;
    background-color: rgba(0, 0, 0, 0.38);
    opacity: 0.6;
  }
`;

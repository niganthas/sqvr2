import React from 'react';
import { Button, Modal } from 'components';
import { DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';

const SocialModal = ({ open, close, login }) => (
  <Modal open={open} onClose={close} aria-labelledby="Условия пользования приложения Сквер">
    <DialogTitle>Условия пользования</DialogTitle>
    <DialogContent>
      <DialogContentText>
        Нажимая “Согласен” вы подтверждаете, что прочли и согласны с{' '}
        <a style={{ color: '#2E87FF' }} href="/">
          политикой конфиденциальности
        </a>{' '}
        и{' '}
        <a style={{ color: '#2E87FF' }} href="/">
          условиями использования
        </a>
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button onClick={close}>Вернуться</Button>
      <Button onClick={() => close() || login()} color="primary">
        Согласен
      </Button>
    </DialogActions>
  </Modal>
);

export default SocialModal;

import React from 'react';
import { baseUrl } from 'utils';
import { IconButton } from '@material-ui/core';
import { withState, compose, withHandlers } from 'recompose';
import config from 'config';
import { Text, grid } from 'components';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import Modal from './Modal';
import * as style from './Social.style';

// http://devsqvr.ru/api/graphql/v1

// https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow
// https://developers.facebook.com/apps/1887872448155024/fb-login/settings/

// https://vk.com/dev/authcode_flow_user

const SOCIALS = {
  twitter: {},
  vk: {
    href: `\
https://oauth.vk.com/authorize\
?client_id=${config.SOCIALS.vk.clientId}\
&display=popup\
&redirect_uri=http://sqvr.loc:3000/registration/callback
&scope=email\
&response_type=code\
&v=5.80\
`,
  },
  google: {},
  facebook: {
    href: `\
https://www.facebook.com/v3.0/dialog/oauth\
?scope=email,user_link\
&client_id=1887872448155024\
&redirect_uri=http://sqvr.loc:3000/registration/callback\
&response_type=token\
&state={state-param}\
`,
  },
};

const awaitWindowClose = window =>
  new Promise(resolve => {
    const checkConnect = setInterval(() => {
      if (!window || !window.closed) return;
      clearInterval(checkConnect);
      resolve();
    }, 300);
  });

const SocialLinks = ({ socialType, setSocialType, register }) =>  (
  <style.Wrapper>
    <style.TextBlock>
      <Text variant="caption" px="10px">
        или через
      </Text>
    </style.TextBlock>
    <grid.Flex justifyContent="space-between" px="16px">
      {Object.keys(SOCIALS).map(social => (
        <IconButton key={social} onClick={() => setSocialType(social)}>
          <img
            src={baseUrl() + `/img/social/${social}.svg`}
            alt={`Войти с помощью аккаунта ${social}`}
          />
        </IconButton>
      ))}
    </grid.Flex>
    <Modal
      login={async () => {
        const win = window.open(SOCIALS[socialType].href, 'soc', 'modal=yes');
        await awaitWindowClose(win);

        const urlData = localStorage.getItem('registration_callback') || '';

        console.log((/access_token=([^&]+)/.exec(urlData) || [])[1]);
        const resp = await register({
          variables: {
            social: {
              socialType,
              accessToken: (/access_token=([^&]+)/.exec(urlData) || [])[1],
              code: (/code=([^&]+)/.exec(urlData) || [])[1],
            },
          },
        });
        console.log(resp);
      }}
      open={!!socialType}
      close={() => setSocialType(null)}
    />
  </style.Wrapper>
);

export default compose(
  withState('socialType', 'setSocialType', null),
  graphql(
    gql`
      mutation register($social: UserSocialRegistrationInput) {
        registration {
          stepOne(social: $social) {
            status
          }
        }
      }
    `,
    { name: 'register' }
  ),
  graphql(
    gql`
      mutation login($social: UserSocialRegistrationInput) {
        registration {
          stepOne(social: $social) {
            status
          }
        }
      }
    `,
    { name: 'login' }
  ),
  withHandlers({})
)(SocialLinks);

import React from 'react';
import styled from 'styled-components';
import { media } from 'utils';
import { Header, grid, StepperDesktop } from 'components';
import { baseUrl } from 'utils';

const Container = styled.div`
  width: 100%;
  min-height: 100vh;
  min-width: 320px;
  background-color: #f7f7f7;

  ${media.phablet`padding-top: 116px;`};

  ${media.desktop`
    display: flex;
    padding: 0;
  `};
`;

const AuthWrapper = styled.div`
  ${media.desktop`
    padding: 32px;
    width: 814px;
    min-height: 615px;
    background: linear-gradient(#2E87FF, #1060CC);
    margin: auto;
    border-radius: 16px;
    display: flex;
    justify-content: space-between;
    box-shadow: 0 32px 48px rgba(38, 50, 56, 0.32);
  `};
`;

const Inner = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  min-height: 100vh;
  padding: 24px;
  padding-bottom: 32px;
  width: 100%;
  margin: 0 auto;
  background-color: #ffffff;

  ${media.phablet`
    max-width: 416px;
    min-height: 550px;
    border-radius: 6px;
    box-shadow: 0px 4px 8px rgba(176, 190, 197, 0.24);
    padding: 24px 48px 48px;
  `};

  ${media.desktop`
    padding: 48px;
    margin: 0;
    box-shadow: 0 16px 24px rgba(38, 50, 56, 0.32);
  `};
`;

const StepperBlock = styled.div`
  display: none;

  ${media.desktop`
    display: block;
    width: 300px;
    min-height: 550px;
  `};
`;

export const Layout = ({ children, title }) => (
  <Container>
    <AuthWrapper>
      <StepperBlock>
        <grid.Box mb="30px" pt="10px" pl="15px">
          <img
            src={baseUrl() + '/img/logo-white.svg'}
            width="120"
            height="39"
            alt="Логотип Сквер"
          />
        </grid.Box>
        <StepperDesktop />
      </StepperBlock>
      <Inner>
        <Header small title={title} />
        {children}
      </Inner>
    </AuthWrapper>
  </Container>
);

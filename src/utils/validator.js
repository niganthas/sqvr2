import * as yup from 'yup';

import {
  blankEmail,
  invalidEmail,
  blankPassword,
  passwordNotLongEnough,
  passwordsMustMatch,
} from './errorMessages';
import { formatYupErrors } from './formatYupErrors';

const userRegisterSchema = yup.object().shape({
  email: yup
    .string()
    .required(blankEmail)
    .email(invalidEmail),
  password: yup
    .string()
    .required(blankPassword)
    .min(4, passwordNotLongEnough)
    .max(32),
  confirmationPassword: yup
    .string()
    .oneOf([yup.ref('password'), null], passwordsMustMatch),
});

export const validateRegisterUserData = async data => {
  try {
    await userRegisterSchema.validate(data, { abortEarly: false });
  } catch (e) {
    return formatYupErrors(e);
  }
};

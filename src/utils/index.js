import { css } from 'styled-components';
import { format } from 'date-fns';
import ru from 'date-fns/locale/ru';
import config from '../config';

const sizes = {
  phone: 320,
  phablet: 420,
  tablet: 768,
  desktop: 1024,
  desktopXL: 1370,
};

// Iterate through the sizes and create a media template
export const media = Object.keys(sizes).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (min-width: ${sizes[label]}px) {
      ${css(...args)};
    }
  `;

  return acc;
}, {});

export const baseUrl = () => {
  return process.env.NODE_ENV !== 'development' ? '/n' : '';
};

export const redirectSubdomain = (name, target = '/polls/main') => {
  return process.env.NODE_ENV !== 'development'
    ? window.location.replace(`http://${name}.${config.DOMAIN}${baseUrl()}${target}`)
    : window.location.reload();
};

export const formatDate = (date, formating) => {
  return format(new Date(date), formating, { locale: ru });
};

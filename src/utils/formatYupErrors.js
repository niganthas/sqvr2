export const formatYupErrors = e => {
  let errors = {};

  e.inner.forEach(e => {
    errors[e.path] = e.message;
  });

  return errors;
};

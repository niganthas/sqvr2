// Auth
export const blankEmail = 'Email не может быть пустым';
export const invalidEmail = 'Неправильный email';
export const blankPassword = 'Пароль не может быть пустым';
export const passwordNotLongEnough = 'Пароль должен содержать минимум 4 знака';
export const passwordsMustMatch = 'Пароли должны совпадать';

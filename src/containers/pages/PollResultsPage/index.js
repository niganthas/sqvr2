import React from 'react';
import SendIcon from '@material-ui/icons/Send';
import PollCard from './PollCard';
import { Amplitude } from '@amplitude/react-amplitude';
import ModalShared from './ModalShared';
import { withState, withProps, compose } from 'recompose';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import { baseUrl } from 'utils';
import { Redirect } from 'react-router-dom';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import {
  Button,
  SideBar,
  MainContent,
  Text,
  SideButton,
  PollsLayout,
  Loader,
  PollUserBlock,
} from 'components';
import { get } from 'lodash';

const PollResults = ({ user, open, setOpen, allPolls, loading, match, history }) => {
  const polls = get(allPolls, 'polls.listPolls') || [];
  const questions = get(allPolls, 'polls.allQuestionsOfPoll') || [];
  const poll = polls.find(el => el.id === parseInt(match.params.id, 10));

  if (loading) {
    return <Loader loading={loading} />;
  }

  return (
    <Amplitude>
      <PollsLayout title="Результаты" user={user} main>
        {!poll && <Text variant="title">Такого опроса не существует</Text>}

        {poll &&
          !poll.isVote &&
          user.id !== poll.owner.id && <Redirect to={`${baseUrl()}/polls/${match.params.id}`} />}

        {poll && (
          <React.Fragment>
            <SideBar>
              <Text variant="subheading" component="h2" mb="12px">
                Результат опроса
              </Text>
              <SideButton
                variant="contained"
                AmplitudeEvent="OwnerReturnedFromResultsToPoll"
                title="на главную"
                icon={<ArrowBackIcon />}
                onClick={() => {
                  history.push(`${baseUrl()}/polls/main`);
                }}
              />

              <SideButton
                variant="contained"
                AmplitudeEvent="UserSharedPollResults"
                title="поделиться"
                icon={<SendIcon />}
                onClick={() => {
                  setOpen(true);
                }}
              />
            </SideBar>
            <MainContent>
              <PollUserBlock
                src={poll.owner.avatarUrl}
                name={`${poll.owner.firstName} ${poll.owner.lastName}`}
                date={poll.dateUpdated}
              />
              <Text mb="8px" px="8px" variant="title">
                {poll.title}
              </Text>

              <Text variant="body2" component="p" mb="32px" px="8px">
                {poll.description}
              </Text>

              {console.log(questions) ||
                questions.map((question, index) => (
                  <PollCard key={index} question={question} index={index} />
                ))}

              <Button w="272px" variant="contained" color="primary" onClick={() => setOpen(true)}>
                Поделиться результатами
              </Button>
              <ModalShared
                open={!!open}
                close={() => {
                  setOpen(false);
                }}
              />
            </MainContent>
          </React.Fragment>
        )}
      </PollsLayout>
    </Amplitude>
  );
};

const results = gql`
  query ResultsQuery($pollId: Int!, $isAll: Boolean!) {
    polls {
      listPolls(isAll: $isAll) {
        id
        title
        description
        dateUpdated
        isVote
        owner {
          id
          firstName
          lastName
          avatarUrl
        }
      }
      voteResult(pollId: $pollId) {
        pollQuestionId
        questionItems {
          count
          percent
          vote
        }
      }
      allQuestionsOfPoll(pollId: $pollId) {
        questionType
        questionItems
        title
        id
      }
    }
  }
`;

export default compose(
  withState('open', 'setOpen', false),
  graphql(results, {
    name: 'allPolls',
    options: p => ({
      variables: {
        isAll: true,
        pollId: p.match.params.id,
      },
    }),
  }),
  withProps(({ allPolls }) => ({
    loading: allPolls.loading,
    /*     results:
      resultsQuery &&
      resultsQuery.polls &&
      resultsQuery.polls.voteResult.map(voteResult => {
        const { questionType } = (resultsQuery.polls.allQuestionsOfPoll || []).find(
          el => el.id === voteResult.pollQuestionId
        );
        return { ...voteResult, questionType };
      }), */
  }))
)(PollResults);

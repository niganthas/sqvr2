import React from 'react';
import { Card, CardActions } from '@material-ui/core';
import styled from 'styled-components';
import { Button, Text, ProgressBar, Loader } from 'components';
import { media, formatDate } from 'utils';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import { get } from 'lodash';
import compareAsc from 'date-fns/compare_asc';
import {Bar, BarChart, ResponsiveContainer, XAxis, LabelList} from 'recharts';

const ResultCard = styled(Card)`
  padding: 24px 16px 16px;
  border-radius: 8px;
  box-shadow: 0px 4px 8px rgba(176, 190, 197, 0.24);
  margin-bottom: 8px;

  ${media.desktop`
    padding: 28px 32px 20px;
    margin-bottom: 16px;
  `};
`;
const ProgressDesc = styled(Text)`
  width: 256px;
  display: flex;
  justify-content: space-between;
  align-items: flex-end;

  ${media.desktop`
    width: 100%;
    margin-bottom: 8px;
  `};
`;
const CardFooter = styled(CardActions)`
  justify-content: flex-end;
  padding: 0;
`;

const PollCard = ({ question, index, questionVote }) => {
  const loading = get(questionVote, 'loading');
  const voteItems = get(questionVote, 'polls.questionVoteDetail.questionItems') || [];
  const totalVotes = voteItems.reduce((a, b) => {
    return a + parseInt(b.count, 10);
  }, 0);
  const getPercent = value => Math.round((value * 100) / totalVotes);

  if (loading) {
    return <Loader loading={loading} />;
  } else {
    if (question.questionType === 'integer_range') {
      const totalRating = voteItems.reduce((a, b) => {
        return a + parseInt(b.vote, 10);
      }, 0);
      const rating = (totalRating / totalVotes).toFixed(1);

        const data = [
            {name: '0', total: 4000, label: '121(10%)'},
            {name: '1', total: 3000, label: '121'},
            {name: '2', total: 2000, label: '121'},
            {name: '3', total: 2780, label: '121'},
            {name: '4', total: 1890, label: '121'},
            {name: '5', total: 2390, label: '121'},
            {name: '6', total: 3490, label: '121'},
            {name: '7', total: 1890, label: '121'},
            {name: '8', total: 2390, label: '121'},
            {name: '9', total: 3490, label: '121'},
        ];

      return (
        <ResultCard>
          <Text variant="body2" component="h3" customcolor="rgba(0, 0, 0, 0.87)" mb="32px">
            {index + 1}
            {'. '}
            {question.title}
          </Text>
            <ResponsiveContainer width={"100%"} height={450}>
                <BarChart data={data}>
                    <XAxis dataKey="name"/>
                    <Bar dataKey="total" fill="#3D7CF6" maxBarSize={380}>
                        <LabelList dataKey="label" position="top" formatter={(args) => args} />
                    </Bar>
                </BarChart>
            </ResponsiveContainer>


          <ProgressDesc variant="body2" component="p" customcolor="rgba(0, 0, 0, 0.87)">
            <span>Средняя оценка</span>
            <span>{rating}</span>
          </ProgressDesc>
          <ProgressBar type={question.questionType} value={rating * 10} />

          <CardFooter>
            <Button size="small" color="primary">
              Подробнее
            </Button>
          </CardFooter>
        </ResultCard>
      );
    } else if (question.questionType === 'date_range') {
      const items = [...voteItems].sort((a, b) => compareAsc(new Date(a.vote), new Date(b.vote)));

      return (
        <ResultCard>
          <Text variant="body2" component="h3" customcolor="rgba(0, 0, 0, 0.87)" mb="32px">
            {index + 1}
            {'. '}
            {question.title}
          </Text>
          {items.map((item, index) => (
            <React.Fragment key={index}>
              <ProgressDesc variant="body2" component="p" customcolor="rgba(0, 0, 0, 0.87)">
                <span>{formatDate(item.vote, 'D MMMM YYYY')}</span>
                <span>{`${getPercent(item.count)}%`}</span>
              </ProgressDesc>
              <ProgressBar type={question.questionType} value={item.percent} />
            </React.Fragment>
          ))}
          <CardFooter>
            <Button size="small" color="primary">
              Подробнее
            </Button>
          </CardFooter>
        </ResultCard>
      );
    } else if (question.questionType === 'time_range') {
      const items = [...voteItems].sort((a, b) => a.vote - b.vote);
      console.log(items);

      return (
        <ResultCard>
          <Text variant="body2" component="h3" customcolor="rgba(0, 0, 0, 0.87)" mb="32px">
            {index + 1}
            {'. '}
            {question.title}
          </Text>
          {items.map((item, index) => (
            <React.Fragment key={index}>
              <ProgressDesc variant="body2" component="p" customcolor="rgba(0, 0, 0, 0.87)">
                <span>{formatDate(question.questionItems[item.vote], 'HH:mm')}</span>
                <span>{`${getPercent(item.count)}%`}</span>
              </ProgressDesc>
              <ProgressBar type={question.questionType} value={item.percent} />
            </React.Fragment>
          ))}
          <CardFooter>
            <Button size="small" color="primary">
              Подробнее
            </Button>
          </CardFooter>
        </ResultCard>
      );
    } else {
      return (
        <ResultCard>
          <Text variant="body2" component="h3" customcolor="rgba(0, 0, 0, 0.87)" mb="32px">
            {index + 1}
            {'. '}
            {question.title}
          </Text>
          {/* -Чекбоксы/радио-*/}
          {question.questionItems.map((item, index) => {
            // в результатах ищем результат с нужным индексом
            const qVote = voteItems.find(v => v.vote === index.toString());
            return (
              <React.Fragment key={index}>
                <ProgressDesc variant="body2" component="p" customcolor="rgba(0, 0, 0, 0.87)">
                  <span>{item}</span>
                  <span>{`${qVote ? getPercent(qVote.count) : 0}%`}</span>
                </ProgressDesc>
                <ProgressBar type={question.questionType} value={qVote ? qVote.percent : 0} />
              </React.Fragment>
            );
          })}
          <CardFooter>
            <Button size="small" color="primary">
              Подробнее
            </Button>
          </CardFooter>
        </ResultCard>
      );
    }
  }
};

const questionVote = gql`
  query questionVote($pollQuestionId: Int!) {
    polls {
      questionVoteDetail(pollQuestionId: $pollQuestionId) {
        pollQuestionId
        questionItems {
          count
          percent
          vote
        }
      }
    }
  }
`;

export default graphql(questionVote, {
  name: 'questionVote',
  options: props => ({ variables: { pollQuestionId: props.question.id } }),
})(PollCard);

import React from 'react';
import { Button, Modal, CopyLink } from 'components';
import { DialogActions, DialogContent, DialogTitle } from '@material-ui/core';

const ModalShared = ({ open, close }) => (
  <Modal open={open} onClose={close}>
    <DialogTitle>Поделиться</DialogTitle>
    <DialogContent>
      <CopyLink value={window.location.href} />
    </DialogContent>
    <DialogActions>
      <Button onClick={close}>вернуться</Button>
    </DialogActions>
  </Modal>
);

export default ModalShared;

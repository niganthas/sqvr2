import React from 'react';
import { PollsLayout, Button, Text, MainContent, SideBar, SideButton } from 'components';
import ClipBoardIcon from 'components/icons/ClipBoardIcon';
import Question from 'components/question';
import { withTryCreatePoll } from 'hocs';
import uuidv1 from 'uuid/v1';
import { addDays } from 'date-fns';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { baseUrl } from 'utils';

const TEMPLATES = {
  DATE: {
    name: 'date',
    title: 'Дата проведения собрания',
    desc: 'Планируется очередное собрание, когда будем проводить?',
    questions: [
      {
        questionType: 'date_range',
        id: uuidv1(),
        title: 'Какая дата будет удобной?',
        questionItems: [new Date(), addDays(new Date(), 30)],
      },
      {
        questionType: 'time_range',
        id: uuidv1(),
        title: 'Какое время будет удобнее?',
        questionItems: [new Date().toString()],
      },
      {
        questionType: 'checkbox',
        id: uuidv1(),
        title: 'Выбор места проведения',
        questionItems: ['Подъезд №1', 'Подъезд №2', 'Подъезд №3'],
      },
    ],
  },
  QUALITY: {
    name: 'discuss',
    title: 'Качество содержания придомовых территорий',
    desc:
      'Мы хотим, чтобы жильцы дома были довольны качеством обслуживания придомовых территорий и знали, что платят деньги не просто так. Чтобы делать работу хорошо, нам нужна ваша обратная связь. Учтем каждый отзыв.',
    questions: [
      {
        questionType: 'integer_range',
        id: uuidv1(),
        title: 'Как оцениваете чистоту на придомовой территории?',
      },
      {
        questionType: 'checkbox',
        id: uuidv1(),
        title: 'Что не устраивает в уборке?',
        questionItems: [
          'Убирают редко. Снег, листья, мусор могу лежать днями',
          'Урны не очищаются вовремя',
          'Проблем нет',
        ],
      },
      {
        questionType: 'integer_range',
        id: uuidv1(),
        title: 'Как оцениваете вывоз мусора на территории?',
      },
      {
        questionType: 'checkbox',
        id: uuidv1(),
        title: 'Какие проблемы с вывозом мусора связаны?',
        questionItems: [
          'Редко вывозят. Баки переполнены',
          'Остается много мусора вокруг баков',
          'Вывозят в неподходящее время. Шум мешает спать',
          'Проблем нет',
        ],
      },
      {
        questionType: 'integer_range',
        id: uuidv1(),
        title: 'Как оцениваете озеленение территории?',
      },
      {
        questionType: 'checkbox',
        id: uuidv1(),
        title: 'Какие проблемы с озеленением существуют?',
        questionItems: ['Некачественный газон', 'Газон редко стригут', 'Проблем нет'],
      },
      {
        questionType: 'integer_range',
        id: uuidv1(),
        title: 'Как оцениваете состояние тротуара на придомовой территории?',
      },
      {
        questionType: 'checkbox',
        id: uuidv1(),
        title: 'Что не так с тротуаром?',
        questionItems: [
          'Плохо уложена плитка (разрушен асфальт)',
          'Газон выше тротуара. После дождей вся грязь на нём.',
          'Плохой водоотвод',
          'Проблем нет',
        ],
      },
      {
        questionType: 'integer_range',
        id: uuidv1(),
        title:
          'Как оцениваете состояние детской площадки, лавочек и других объектов общего пользования?',
      },
      {
        questionType: 'checkbox',
        id: uuidv1(),
        title: 'Какие есть проблемы с объектами общего пользования?',
        questionItems: [
          'Плохо уложена плитка (разрушен асфальт)',
          'Газон выше тротуара. После дождей вся грязь на нём.',
          'Плохой водоотвод',
          'Проблем нет',
        ],
      },
    ],
  },
  PETS: {
    name: 'pets',
    title: 'Брать ли дополнительную плату за домашних питомцев?',
    desc:
      'В нашем доме многие собственники имеют домашних питомцев, которые гуляют на придомовой территории, ходят по лестницам, ездят в лифтах. Даже самые аккуратные и внимательные хозяева таких питомцев не могут предотвратить ущерб наносимый стенам, полам и дверям в нашем доме. Грязные следы от лап, пятна от мочи, поцарапанные деревянные элементы, требуют дополнительных затрат как на уборку, так и на восстановление. В связи с этим на обсуждение выносится вопрос о дополнительных взносах на каждого домашнего питомца. ',
    questions: [
      {
        questionType: 'select',
        id: uuidv1(),
        title: 'Сколько у Вас домашних питомцев?',
        questionItems: ['Нет ни одного', 'Один', 'Двое', 'Три или больше'],
      },
      {
        questionType: 'select',
        id: uuidv1(),
        title: 'Какую плату в месяц за одного домашнего питомца Вы считаете справедливой?',
        questionItems: ['Нисколько не брать', '500 рублей', '100 рублей', 'Более 500 рублей'],
      },
      {
        questionType: 'select',
        id: uuidv1(),
        title: 'Когда лучше всего начать получать дополнительную плату за домашних питомцев?',
        questionItems: [
          'Немедленно',
          'Начиная со следующего года',
          'Предусмотреть поэтапное внедрение в течение нескольких лет',
        ],
      },
      {
        questionType: 'select',
        id: uuidv1(),
        title: 'Как часто Вы сталкиваетесь с проблемами, связанными с домашними питомцами?',
        questionItems: ['Ни разу небыло проблем', 'Редко', 'Регулярно'],
      },
    ],
  },
  SECURITY: {
    name: 'date',
    title: 'Вы за домофон или консьержа?',
    desc:
      'В нашем доме уже много лет постоянно дежурит консьерж и нет домофона. Это приводит к тому, что в ночное время, когда вероятность неприятностей выше, подъезд открыт и находится без присмотра. Содержание консьержа обходится в 10 тыс. рублей в месяц или 120 тыс.рублей в год, не считаю расходов по уборке и содержанию помещения. Установка домофона и ежегодное обслуживание обойдется нам в 16 тыс. И 5 тыс.рублей соответственно. Кроме того освободится помещение для консьержа, которое можно использовать как место хранения велосипедов или детских колясок.',
    questions: [
      {
        questionType: 'select',
        id: uuidv1(),
        title: 'Что Вам больше нравится?',
        questionItems: ['Консьерж', 'Домофон', 'И домофон и консьерж вместе', 'Ни то, ни другое'],
      },
      {
        questionType: 'select',
        id: uuidv1(),
        title: 'Что Вам больше всего нравится в варианте консьерж?',
        questionItems: [
          'Живое общение',
          'Возможность попросить о чем-то. Получить помощь',
          'Что человек может правильно отреагировать на проблемную ситуацию',
        ],
      },
      {
        questionType: 'select',
        id: uuidv1(),
        title: 'Что Вам больше всего НЕ нравится в варианте “Консьерж”?',
        questionItems: [
          'Что он не всегда на месте',
          'В помещении консьержа не всегда чисто',
          'Цена. Дорого',
        ],
      },
      {
        questionType: 'select',
        id: uuidv1(),
        title: 'Что Вам больше всего нравится в варианте “Домофон”?',
        questionItems: [
          'Меньше затрат',
          'Можно обойтись без людей',
          'Дверь круглосуточно под контролем',
        ],
      },
      {
        questionType: 'select',
        id: uuidv1(),
        title: 'Что Вам больше всего НЕ нравится в варианте “Домофон”?',
        questionItems: [
          'Часто ломается и потом долго не ремонтируется',
          'В подъезд может попасть любой, кто узнал код',
          'Нужно запоминать еще один пароль',
        ],
      },
      {
        questionType: 'select',
        id: uuidv1(),
        title: 'Вы за “Домофон” или за “Консьерж”?',
        questionItems: ['За “Домофон”', 'За “Консьерж”', 'Воздержался'],
      },
    ],
  },
};

const ViewTemplate = ({ match, user, history, createPoll }) => {
  const template = TEMPLATES[match.params.template.toUpperCase()];

  return (
    <PollsLayout title="Шаблон" user={user}>
      <SideBar>
        <Text variant="subheading" component="h2" mb="12px">
          Шаблон
        </Text>
        <SideButton
          AmplitudeEvent="UserUsedTemplate"
          variant="contained"
          title="выбор шаблона"
          icon={<ArrowBackIcon />}
          onClick={() => history.push(`${baseUrl()}/polls/templates`)}
        />
        <SideButton
          AmplitudeEvent="UserUsedTemplate"
          variant="contained"
          title="Использовать"
          icon={<ClipBoardIcon />}
          onClick={() => createPoll(template)}
        />
      </SideBar>
      <MainContent>
        <Text variant="title" component="h2" mb="15px">
          {template.title}
        </Text>
        <Text variant="body2" component="p" mb="15px">
          {template.desc}
        </Text>
        {template.questions.map((question, index) => (
          <Question key={index} index={index} {...question} />
        ))}
        <Button
          AmplitudeEvent="UserUsedTemplate"
          type="submit"
          onClick={props => {
            console.log(template);
            createPoll(template);
          }}
          variant="contained"
          color="primary"
          mt="auto"
          mb="26px"
        >
          Использовать шаблон
        </Button>
      </MainContent>
    </PollsLayout>
  );
};

export default withTryCreatePoll(ViewTemplate);

import React from 'react';
import InsertInvitation from '@material-ui/icons/InsertInvitation';
import { PollsLayout, TemplateButton, SideBar, MainContent, Text, SideButton } from 'components';
import Assignment from '@material-ui/icons/Assignment';
import ThumbUp from '@material-ui/icons/ThumbUp';
import Pets from '@material-ui/icons/Pets';
import Home from '@material-ui/icons/Home';
import ClipBoardIcon from 'components/icons/ClipBoardIcon';
import { Amplitude } from '@amplitude/react-amplitude';
import styled from 'styled-components';
import { media } from 'utils';
import { withTryCreatePoll } from 'hocs';

const Buttons = styled.div`
  ${media.desktop`
    display: flex;
    justify-content: flex-start;
    flex-wrap: wrap;
  `};
`;

const templates = [
  {
    link: '/polls/templates/date',
    title: 'Договориться о встрече',
    desc: 'Готовые вопросы с выбором даты, временем и местом проведения',
    icon: <InsertInvitation />,
  },
  {
    link: '/polls/templates/quality',
    title: 'Оценить услуги',
    desc: 'Качество содержания придомовых территорий',
    icon: <ThumbUp />,
  },
  {
    link: '/polls/templates/pets',
    title: 'Домашние животные',
    desc: 'Брать ли плату за домашних питомцев?',
    icon: <Pets />,
  },
  {
    link: '/polls/templates/security',
    title: 'Безопасность подъезда',
    desc: 'Вы за домофон или консьержа?',
    icon: <Home />,
  },
];

const PollTemplatesPage = ({ user, createPoll }) => (
  <Amplitude
    eventProperties={{
      scope: ['PollsTemplatesPage'],
    }}
  >
    <PollsLayout title="Шаблоны" user={user} main>
      <SideBar>
        <Text variant="subheading" component="h2" mb="12px">
          Создание опроса
        </Text>

        <SideButton
          AmplitudeEvent="UserEnterOnCreatePoll"
          variant="contained"
          title="Пустой"
          icon={<ClipBoardIcon />}
          onClick={() => createPoll()}
        />

        <SideButton variant="contained" color="primary" title="Из шаблона" icon={<Assignment />} />
      </SideBar>
      <MainContent>
        <Buttons>
          {templates.map((template, index) => (
            <TemplateButton
              key={index}
              AmplitudeEvent={`ClickOnTemplate "${template.title}"`}
              to={template.link}
              title={template.title}
              desc={template.desc}
            >
              {template.icon}
            </TemplateButton>
          ))}
        </Buttons>
      </MainContent>
    </PollsLayout>
  </Amplitude>
);

export default withTryCreatePoll(PollTemplatesPage);

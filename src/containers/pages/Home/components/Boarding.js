import React from 'react'; 
import {Stepper} from 'components';
import Layout from './Layout';

const Boarding = () => (
  <Layout>
    <Stepper />
  </Layout>
)

export default Boarding;
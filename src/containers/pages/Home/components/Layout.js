import React from 'react';
import styled from 'styled-components';
import { media } from 'utils';

const Container = styled.div`
  width: 100%;
  max-width: 364px;
  margin: 0 auto;
  padding: 25px;
  min-height: 100vh;
  display: flex;
  flex-direction: column;

  ${media.phablet`min-height: auto; padding-top: 120px;`}
`;

const Layout = ({ children }) => (
  <Container>
    {children}
  </Container>
);

export default Layout;
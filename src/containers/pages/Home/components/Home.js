import React from 'react';
import { Button, Header, Footer } from 'components';
import * as grid from 'components/grid';
import Layout from './Layout';
import { Modal, withStyles } from '@material-ui/core';
import { baseUrl } from 'utils';
import { compose, withState } from 'recompose';
import Assignment from '@material-ui/icons/Assignment';
import ClipBoardIcon from 'components/icons/ClipBoardIcon';
import { withTryCreatePoll } from 'hocs';

const styles = {
  modal: {
    backgroundColor: '#ffffff',
    height: 115,
    width: '100%',
    zIndex: 10,
    position: 'absolute',
    bottom: 0,
    paddingTop: 8,
  },
  button: {
    width: '100%',
    height: 48,
    marginBottom: 4,
    textTransform: 'none',
    justifyContent: 'flex-start',
  },
  icon: {
    color: '#757575',
    marginRight: 32,
  },
};

const Home = ({ open, setOpen, classes, createPoll }) => (
  <Layout>
    <Header />
    <grid.Box mt="auto" mb="30px" ml="auto" mr="auto">
      <img src={baseUrl() + '/img/polls/home.svg'} width="270" height="256" alt="Сквер Опросы" />
    </grid.Box>
    <Button
      variant="contained"
      color="primary"
      onClick={() => {
        setOpen(!open);
      }}
      mb="15px"
    >
      Создать опрос
    </Button>
    <Button variant="outlined" mb="19px" color="primary" to="/auth/login">
      Войти
    </Button>
    <Footer />
    <Modal
      aria-labelledby="Выбора шаблона опроса"
      open={open}
      onClose={() => {
        setOpen(!open);
      }}
    >
      <div className={classes.modal}>
        <Button
          AmplitudeEvent="UserEnterOnCreatePollFromHome"
          onClick={createPoll}
          className={classes.button}
        >
          <ClipBoardIcon className={classes.icon} />
          Пустой
        </Button>
        <Button
          AmplitudeEvent="UserUsedTemplateFromHome"
          to="/polls/templates"
          className={classes.button}
        >
          <Assignment className={classes.icon} />
          Из шаблонов
        </Button>
      </div>
    </Modal>
  </Layout>
);

export default compose(
  withTryCreatePoll,
  withStyles(styles),
  withState('open', 'setOpen', false)
)(Home);

import React from 'react';
import { IconButton } from '@material-ui/core';
import {
  PollsLayout,
  Text,
  Button,
  SideBar,
  MainContent,
  SideButton,
  Loader,
  PollUserBlock,
  Question,
} from 'components';
import { withState, compose } from 'recompose';
import ModalPublish from './ModalPublish';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import { baseUrl } from 'utils';
import { get } from 'lodash';

import AssignmentIcon from '@material-ui/icons/AssignmentTurnedIn';
import EditIcon from '@material-ui/icons/Edit';
import SendIcon from '@material-ui/icons/Send';

const PollPublication = ({ open, setOpen, user, allPolls, setStatus, history, match }) => {
  const polls = get(allPolls, 'polls.listPolls') || [];
  const loading = get(allPolls, 'loading');
  const questions = get(allPolls, 'polls.allQuestionsOfPoll') || [];
  const poll = polls.find(poll => poll.status === 'draft');

  if (loading) {
    return <Loader loading={loading} />;
  }

  return (
    <PollsLayout
      title="Предпросмотр"
      icon={
        <IconButton onClick={() => setOpen(!open)}>
          <SendIcon />
        </IconButton>
      }
      user={user}
      onBackButtonClick={() => {
        history.push(`${baseUrl()}/polls/${match.params.id}/create-poll`);
      }}
    >
      {!poll && <Text variant="title">Такого опроса не существует</Text>}

      {poll && (
        <React.Fragment>
          <SideBar>
            <Text variant="subheading" component="h2" mb="12px">
              Предпросмотр
            </Text>
            <SideButton
              AmplitudeEvent="UserReturnOnCreateFromPublish"
              to={`/polls/${match.params.id}/create-poll`}
              variant="contained"
              title="редактировать"
              icon={<EditIcon />}
            />
            <SideButton
              AmplitudeEvent="UserClickOnPublish"
              variant="contained"
              title="опубликовать"
              icon={<AssignmentIcon />}
              onClick={() => setOpen(!open)}
            />
          </SideBar>
          <MainContent>
            <PollUserBlock
              src={poll.owner.avatarUrl}
              name={`${poll.owner.firstName} ${poll.owner.lastName}`}
              date={poll.dateUpdated}
            />
            <Text variant="title" mb="8px">
              {poll.title}
            </Text>
            <Text mb="25px" component="p" variant="caption">
              {poll.description}
            </Text>
            {questions.map((question, index) => (
              <Question key={index} index={index} {...question} />
            ))}
            <Button
              AmplitudeEvent="UserClickOnPublish"
              w="272px"
              my="30px"
              variant="contained"
              color="primary"
              onClick={() => setOpen(!open)}
            >
              Опубликовать
            </Button>
          </MainContent>

          <ModalPublish
            open={open}
            close={() => {
              setOpen(!open);
            }}
            onSuccess={async () => {
              await setStatus({ variables: { status: 'active', pollId: poll.id } });
              history.push(`${baseUrl()}/polls/${poll.id}`);
            }}
          />
        </React.Fragment>
      )}
    </PollsLayout>
  );
};

const pollsQuery = gql`
  query AllPollsQuery($isAll: Boolean, $pollId: Int!) {
    polls {
      listPolls(isAll: $isAll) {
        id
        dateCreated
        dateUpdated
        title
        description
        status
        owner {
          firstName
          lastName
          avatarUrl
        }
      }
      allQuestionsOfPoll(pollId: $pollId) {
        id
        title
        questionType
        questionItems
      }
    }
  }
`;

export default compose(
  withState('open', 'setOpen', false),
  graphql(pollsQuery, {
    name: 'allPolls',
    options: props => ({
      fetchPolicy: 'network-only',
      variables: { isAll: true, pollId: props.match.params.id },
    }),
  }),
  graphql(
    gql`
      mutation SetStatus($status: String!, $pollId: Int!) {
        polls {
          setStatusPoll(status: $status, pollId: $pollId) {
            status
          }
        }
      }
    `,
    {
      name: 'setStatus',
    }
  )
)(PollPublication);

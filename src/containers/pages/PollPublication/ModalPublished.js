import React from 'react';
import { Button, Modal, Text, grid, CopyLink } from 'components';
import { withStyles, DialogActions, DialogContent, DialogTitle, Icon } from '@material-ui/core';
import CheckIcon from '@material-ui/icons/CheckCircle';
import { compose, withState } from 'recompose';

const styles = {
  title: {
    color: '#00DA71',
  },
  icon: {
    color: '#00DA71',
  },
};

const ModalPublished = props => {
  const { classes, open, setOpen } = props;
  return (
    <Modal open={open} onClose={setOpen(false)}>
      <DialogTitle>
        <grid.Flex alignItems="center">
          <Icon style={{ marginRight: 8 }}>
            <CheckIcon className={classes.icon} />
          </Icon>
          <Text variant="title" component="span" className={classes.title}>
            Опрос опубликован
          </Text>
        </grid.Flex>
      </DialogTitle>
      <DialogContent>
        <Text variant="body2" mb="32px">
          Теперь вы можете поделиться опросом с помощью ссылки
        </Text>
        <CopyLink value={window.location.href} />
      </DialogContent>
      <DialogActions>
        <Button onClick={setOpen(false)} color="primary">
          Перейти к опросу
        </Button>
      </DialogActions>
    </Modal>
  );
};

export default compose(
  withStyles(styles),
  withState('open', 'setOpen', true)
)(ModalPublished);

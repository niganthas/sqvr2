import React from 'react';
import { Button, Modal, Text } from 'components';
import { DialogActions, DialogContent, DialogTitle } from '@material-ui/core';

const ModalPublish = ({ open, close, onSuccess }) => (
  <Modal open={open} onClose={close}>
    <DialogTitle>Опубликовать опрос?</DialogTitle>
    <DialogContent>
      <Text variant="body2" mb="32px">
        После публикации опроса вы не сможете вносить изменения.
      </Text>
    </DialogContent>
    <DialogActions>
      <Button onClick={close}>Вернуться</Button>
      <Button
        AmplitudeEvent="PollSuccessPublish"
        onClick={() => {
          onSuccess();
          close();
        }}
        color="primary"
      >
        Опубликовать
      </Button>
    </DialogActions>
  </Modal>
);

export default ModalPublish;

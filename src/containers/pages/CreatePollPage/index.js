import React from 'react';
import { IconButton } from '@material-ui/core';
import Add from '@material-ui/icons/Add';
import {
  Button,
  SideBar,
  MainContent,
  Text,
  SideButton,
  PollsLayout,
  grid,
  fields,
} from 'components';
import { compose } from 'recompose';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import { get } from 'lodash';
import SendIcon from '@material-ui/icons/Send';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { baseUrl, media } from 'utils';
import { Amplitude } from '@amplitude/react-amplitude';
import uuidv1 from 'uuid/v1';
import QuestionsList from './QuestionsList';
import { withApollo } from 'react-apollo';
import DeleteIcon from '@material-ui/icons/Delete';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import styled from 'styled-components';

const Buttons = styled.div`
  display: flex;
  justify-content: center;
  position: absolute;
  bottom: 0;
  right: 0;
  left: 0;
  padding: 40px 16px;
  background: linear-gradient(rgba(255, 255, 255, 0), #ffffff);

  ${media.desktop`
    bottom: -32px;
    background: transparent;
  `};
`;

const StyledButton = styled(Button)`
  border-radius: 18px;
  font-size: 12px;
  line-height: 12px;
  width: 40%;
  text-transform: none;
  padding: 12px;
`;

class CreatePollPage extends React.Component {
  state = {
    template: false,
    pollName: '',
    pollDescription: '',
    pollNameError: false,
    pollDescriptionError: false,
    pollId: null,
    questions: [],
  };

  async componentWillMount() {
    console.log(this.props);
    const polls = await this.props.client.query({
      query: pollsQuery,
      fetchPolicy: 'network-only',
      variables: { isAll: false, pollId: this.props.match.params.id },
    });
    this.updateState(polls.data);

    const template = get(this.props.location, 'state.template');

    if (template) {
      this.setState({
        template: template,
        questions: template.questions,
        pollName: template.title,
        pollDescription: template.desc,
      });
      this.onDataSave();
    }

    if (!this.state.questions.length) {
      this.onQuestionAdd();
    }
  }

  updateState = polls => {
    // текущий запрос
    const poll = polls.polls.listPolls.filter(
      poll => poll.id === parseInt(this.props.match.params.id, 10)
    )[0];
    // его вопросы
    const pollQuestions = polls.polls.allQuestionsOfPoll;

    this.setState({
      pollName: poll.title,
      pollId: poll.id,
      pollDescription: poll.description,
      questions: [...pollQuestions],
    });
  };

  formValidation = () => {
    const questionsValidTitle = this.state.questions.filter(q => {
      return q.title;
    });

    // проверяем название вопроса
    if (!this.state.pollName) {
      this.setState({
        pollNameError: true,
      });
      return false;
    }
    // проверяем правильность заполнения вопросов
    if (questionsValidTitle.length !== this.state.questions.length) {
      alert('У всех вопросов должно быть название');
      return false;
    }
    return true;
  };
  // публикация вопроса - валидация + сохранение + переход на предпросмотр
  onQuestionPublish = async e => {
    e.preventDefault();

    if (!this.formValidation()) {
      return;
    }

    await this.onDataSave();

    this.props.history.push(baseUrl() + '/polls/' + this.state.pollId + '/publish');
  };

  // сохраняем данные на сервер
  onDataSave = async () => {
    //TODO: проверить, изменились ли данные перед отправкой
    const pollUpdate = await this.props.updatePoll({
      variables: {
        id: this.state.pollId,
        title: this.state.pollName,
        description: this.state.pollDescription,
      },
    });
    console.log(pollUpdate, 'Обновили опрос');

    if (pollUpdate.error) {
      alert(pollUpdate.error.message);
      return;
    }

    const questions = this.state.questions.map(question => {
      return {
        id: typeof question.id === 'number' ? question.id : null,
        title: question.title,
        questionType: question.questionType,
        questionItems: question.questionItems,
        attachments: question.attachments,
      };
    });

    const questionsUpdate = await this.props.updateQuestions({
      variables: {
        pollId: this.state.pollId,
        questions: questions,
      },
    });
    console.log(questionsUpdate, 'Обновили вопросы');

    if (questionsUpdate.error) {
      return alert(questionsUpdate.error.message);
    }
  };

  onPollDataChanged = e => {
    if (e.target.value.length < 1) {
      this.setState({
        [e.target.name + 'Error']: true,
      });
    } else {
      this.setState({
        [e.target.name + 'Error']: false,
      });
    }
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  onQuestionAdd = () => {
    const questions = this.state.questions;
    questions.push({
      title: '',
      attachments: [],
      questionType: 'select',
      questionItems: [],
      id: uuidv1(),
    });

    this.setState({ questions });
  };

  onQuestionDelete = id => {
    console.log('delete q', id);
    const { questions } = this.state;
    const result = questions.filter(question => question.id !== id);
    this.setState({ questions: result });
  };

  onQuestionChange = data => {
    const questions = this.state.questions.map(
      question => (question.id === data.id ? data : question)
    );

    this.setState({ questions });
  };

  render() {
    const { user, deletePoll, history } = this.props;
    const {
      questions,
      pollName,
      pollDescription,
      pollNameError,
      pollDescriptionError,
    } = this.state;

    return (
      <Amplitude
        eventProperties={{
          scope: ['CreatePollPage'],
        }}
      >
        <PollsLayout
          title="Создание опроса"
          user={user}
          icon={
            <IconButton onClick={this.onQuestionPublish}>
              <SendIcon />
            </IconButton>
          }
          onBackButtonClick={() => {
            history.goBack();
          }}
        >
          <SideBar>
            <Text variant="subheading" component="h2" mb="12px">
              Создание опроса
            </Text>
            <SideButton
              variant="contained"
              title="вернуться"
              icon={<ArrowBackIcon />}
              AmplitudeEvent="UserReturnFromPollCreate"
              onClick={() => {
                history.push(`${baseUrl()}/polls/main`);
              }}
            />
            <SideButton
              variant="contained"
              title="предпросмотр"
              icon={<VisibilityIcon />}
              AmplitudeEvent="UserPollPreview"
              onClick={this.onQuestionPublish}
            />
            <SideButton
              variant="contained"
              title="удалить"
              icon={<DeleteIcon />}
              AmplitudeEvent="UserDeletedNotPublishPoll"
              onClick={async () => {
                await deletePoll();
                history.push(`${baseUrl()}/polls/main`);
              }}
            />
          </SideBar>
          <MainContent>
            <grid.Box pb="90px">
              <grid.Box px="8px">
                <fields.Text
                  name="pollName"
                  onChange={this.onPollDataChanged}
                  value={pollName}
                  placeholder="Введите название"
                  label="Название"
                  error={pollNameError}
                  fullWidth
                  multiline
                  rowsMax={5}
                  onBlur={this.onDataSave}
                  mb="16px"
                />
                <fields.Text
                  name="pollDescription"
                  onChange={this.onPollDataChanged}
                  value={pollDescription}
                  placeholder="Введите описание"
                  label="Описание"
                  error={pollDescriptionError}
                  multiline
                  fullWidth
                  rowsMax={15}
                  onBlur={this.onDataSave}
                  mb="16px"
                />
                <Text variant="subheading" component="h3" mb="16px" mt="24px">
                  Вопросы
                </Text>
              </grid.Box>

              <QuestionsList
                questions={questions}
                onDataSave={this.onDataSave}
                onDelete={this.onQuestionDelete}
                onChange={this.onQuestionChange}
              />
            </grid.Box>

            <Buttons>
              <grid.Flex justifyContent="space-between">
                <StyledButton
                  AmplitudeEvent="AddQuestionInPoll"
                  mr="8px"
                  variant="contained"
                  onClick={this.onQuestionAdd}
                >
                  <Add style={{ fontSize: 25, paddingRight: 8 }} />
                  Добавить вопрос
                </StyledButton>

                <StyledButton
                  AmplitudeEvent="UserPollPreview"
                  style={{ width: '55%' }}
                  variant="contained"
                  color="primary"
                  onClick={this.onQuestionPublish}
                >
                  Предпросмотр
                  <SendIcon style={{ fontSize: 25, paddingLeft: 8 }} />
                </StyledButton>
              </grid.Flex>
            </Buttons>
          </MainContent>
        </PollsLayout>
      </Amplitude>
    );
  }
}

const pollsQuery = gql`
  query AllPollsQuery($isAll: Boolean, $pollId: Int!) {
    polls {
      listPolls(isAll: $isAll) {
        id
        dateCreated
        title
        description
        status
      }
      allQuestionsOfPoll(pollId: $pollId) {
        id
        title
        questionType
        questionItems
        attachments {
          id
          path
          mimetype
          name
        }
      }
    }
  }
`;

const updatePollMutation = gql`
  mutation($description: String!, $title: String!, $id: Int!) {
    polls {
      updatePoll(description: $description, title: $title, id: $id) {
        id
      }
    }
  }
`;

const updateQuestionsMutation = gql`
  mutation($pollId: Int!, $questions: [PollQuestionInput]!) {
    polls {
      updatePollQuestions(pollId: $pollId, questions: $questions) {
        questions {
          id
          title
          questionType
          questionItems
        }
      }
    }
  }
`;

const deletePoll = gql`
  mutation DeletePoll($pollId: Int!) {
    polls {
      deletePoll(pollId: $pollId) {
        status
      }
    }
  }
`;

export default compose(
  withApollo,
  /*   graphql(pollsQuery, {
    name: 'allPolls',
    options: props => ({
      variables: { isAll: false, pollId: props.match.params.id },
    }),
  }), */
  graphql(updatePollMutation, { name: 'updatePoll' }),
  graphql(updateQuestionsMutation, { name: 'updateQuestions' }),
  graphql(deletePoll, {
    name: 'deletePoll',
    options: props => ({
      variables: { pollId: props.match.params.id },
    }),
  })
)(CreatePollPage);

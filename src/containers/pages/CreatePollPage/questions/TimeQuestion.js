import React from 'react';
import { FormGroup } from '@material-ui/core';
import { Text, TimePicker } from 'components';
import moment from 'moment';

const TimeQuestion = ({ items, onChange, onDataSave }) => (
  <React.Fragment>
    <Text variant="caption" component="p" mb="4px">
      Диапазон
    </Text>
    <FormGroup row style={{ justifyContent: 'space-between', marginBottom: 16 }}>
      <TimePicker
        style={{ flexBasis: '47%' }}
        showSecond={false}
        minuteStep={30}
        value={moment(items[0]) || moment()}
        onChange={(value, name = 'questionTimeTypeOption-0') => onChange(value, name)}
        name="questionTimeTypeOption-0"
        onBlur={onDataSave}
        disabledHours={() => [0, 1, 2, 3, 4, 5, 6, 23]}
        hideDisabledOptions
      />
      <TimePicker
        style={{ flexBasis: '47%' }}
        showSecond={false}
        minuteStep={30}
        value={moment(items[items.length - 1]) || moment()}
        onChange={(value, name = 'questionTimeTypeOption-1') => onChange(value, name)}
        name="questionTimeTypeOption-1"
        onBlur={onDataSave}
        disabledHours={() => [0, 1, 2, 3, 4, 5, 6, 23]}
        hideDisabledOptions
      />
    </FormGroup>
  </React.Fragment>
);

export default TimeQuestion;

import React from 'react';
import { FormGroup } from '@material-ui/core';
import { Text } from 'components';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { withState, compose } from 'recompose';
import styled from 'styled-components';
import 'react-datepicker/dist/react-datepicker.css';
import { media } from 'utils';

const Picker = styled(DatePicker)`
  font-family: Montserrat, Arial, sans-serif;
  border: 0;
  border-bottom: 1px solid rgba(0, 0, 0, 0.42);
  padding: 6px 0;
  font-size: 14px;
  font-family: Montserrat, Arial, sans-serif;
  border-radius: 0;
  background-color: transparent;
  width: 100%;

  &:focus {
    border-color: #2e87ff;
  }
`;
const Wrapper = styled.div`
  width: 100%;

  & .react-datepicker-wrapper {
    width: 100%;
  }
  & .react-datepicker__input-container {
    width: 100%;
  }

  ${media.desktop`width: 47%;`};
`;
const DateRangeQuestion = ({
  onChange,
  onDataSave,
  firstDate,
  setFirstDate,
  lastDate,
  setLastDate,
}) => (
  <React.Fragment>
    <Text variant="caption" component="p" mb="4px">
      Диапазон
    </Text>

    <FormGroup row style={{ justifyContent: 'space-between', marginBottom: 16 }}>
      <Wrapper>
        с
        <Picker
          name="questionDateItem-0"
          dateFormat="DD.MM.YYYY"
          selected={firstDate}
          onBlur={onDataSave}
          onChange={(value, e, name = 'questionDateItem-0') => {
            setFirstDate(value);
            onChange(value, name);
          }}
        />
      </Wrapper>

      <Wrapper>
        по
        <Picker
          name="questionDateItem-1"
          dateFormat="DD.MM.YYYY"
          selected={lastDate}
          onBlur={onDataSave}
          onChange={(value, e, name = 'questionDateItem-1') => {
            setLastDate(value);
            onChange(value, name);
          }}
        />
      </Wrapper>
    </FormGroup>
  </React.Fragment>
);

export default compose(
  withState('firstDate', 'setFirstDate', p => moment(p.items[0])),
  withState('lastDate', 'setLastDate', p => moment(p.items[1]))
)(DateRangeQuestion);

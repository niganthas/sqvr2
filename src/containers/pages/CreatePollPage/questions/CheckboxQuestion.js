import React from 'react';
import { TextField } from '@material-ui/core';
import { compact } from 'lodash';

const fillItems = items => {
  // если меньше двух элементов то заполнять пустыми строками
  // если последдний элемент не пустая строка - пушить пустую строку
  items = compact(items);
  if (items[items.length - 1] !== '') {
    items.push('');
  }
  if (items.length < 2) {
    items.push('');
  }
  return items;
};

const CheckboxQuestion = ({ items, onChange, namePrefix, onDataSave }) =>
  fillItems(items).map((item, index) => (
    <TextField
      key={index}
      onChange={onChange}
      value={item}
      name={`${namePrefix}-${index}`}
      label={`Вариант ${index + 1}`}
      style={{ marginBottom: '16px' }}
      fullWidth
      multiline
      rowsMax={5}
      onBlur={onDataSave}
    />
  ));

export default CheckboxQuestion;

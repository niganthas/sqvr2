import React from 'react';
import DateRangeQuestion from './DateRangeQuestion';
import CheckboxQuestion from './CheckboxQuestion';
import TimeQuestion from './TimeQuestion';
import { Button } from 'components';
import { formatDate } from 'utils';
import { addMinutes } from 'date-fns';

import {
  Card,
  TextField,
  CardActions,
  CardContent,
  withStyles,
  ListItemIcon,
  ListItemText,
  MenuItem,
} from '@material-ui/core';

import TimeIcon from '@material-ui/icons/AccessTime';
import DateIcon from '@material-ui/icons/InsertInvitation';
import VariantIcon from '@material-ui/icons/RadioButtonChecked';
import EstimateIcon from '@material-ui/icons/Poll';
import MultipleIcon from '@material-ui/icons/CheckBox';
import DeleteIcon from '@material-ui/icons/Delete';
import UploadIcon from '@material-ui/icons/AttachFile';
import gql from 'graphql-tag';
import { compose } from 'recompose';
import { withApollo } from 'react-apollo';
import styled from "styled-components";
import {Close} from "@material-ui/icons";
import get from "lodash/get";

const styles = {
  card: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    minWidth: 288,
    width: '100%',
    minHeight: 200,
    padding: '24px 16px 0',
    borderRadius: 6,
    marginBottom: 10,
    boxShadow: '0 4px 8px rgba(176, 190, 197, 0.24)',
  },
  cardContent: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    padding: 0,
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '12px 0',
    margin: '0 -16px',
  },
  menuItem: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  textField: {
    marginBottom: 16,
  },
  header: {
    marginBottom: 16,
  },
  button: {
    padding: 0,
    margin: 0,
    minWidth: 48,
    width: 48,

    '& svg': {
      color: '#757575',
    },
  },
  desktopOnly: {
    display: 'none',
  },
  fileInput: {
    display: 'none',
  },
  '@media (min-width: 1024px)': {
    card: {
      minHeight: 150,
    },
    button: {
      width: 'auto',
      padding: '0 16px',
    },
    desktopOnly: {
      display: 'block',
    },
    header: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'flex-end',
      marginBottom: 24,
    },
    selectField: {
      marginBottom: 0,
      width: 280,
      flexShrink: 0,
      marginLeft: 40,
    },
    textField: {
      marginBottom: 0,
    },
  },
};

const types = [
  {
    label: 'Дата',
    type: 'date_range',
    icon: <DateIcon />,
  },
  {
    label: 'Один из вариантов',
    type: 'select',
    icon: <VariantIcon />,
  },
  {
    label: 'Оценить от 1 до 10',
    type: 'integer_range',
    icon: <EstimateIcon />,
  },
  {
    label: 'Множественный выбор',
    type: 'checkbox',
    icon: <MultipleIcon />,
  },
  {
    label: 'Время',
    type: 'time_range',
    icon: <TimeIcon />,
  },
];

const Images = styled('div')`
  display: flex;
  margin-bottom: 16px;
`;

const ImageWrapper = styled('div')`
  :not(:first-child) {
    margin-left: 20px;
  }
  position: relative;
  width: 224px;
  height: 144px;
  flex-shrink: 0;
  img {
    width: 100%;
    height: 100%;
  }
`;

const RemoveImage = styled(Button)`
  border-radius: 2px;
  min-height: 0;
  min-width: 0;
  width: 18px;
  height: 18px;
  position: absolute;
  right: 4px;
  top: 4px;
  svg {
    width: 10px;
    height: 10px;
  }
`;



const FileChip = styled('div')`
  margin-top: 8px;
  padding: 8px;
  width: 240px;
  height: 36px;
  background: rgba(0, 0, 0, 0.04);
  border-radius: 3px;
  display: flex;
  flex-wrap: nowrap;
  align-items: center;
  svg {
    cursor: pointer;
    margin-left: auto;
  }
`;

const FileName = styled('div')`
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
`;

const ALLOWED_PICTURE_EXTENSIONS = ['png', 'jpg', 'jpeg', 'bmp', 'tif', 'tiff', 'tga', 'gif'];

const ALLOWED_AUDIO_EXTENSIONS = ['mp3', 'wma', 'amr', 'ogg', 'aac', 'wav', 'flac'];
const ALLOWED_VIDEO_EXTENSIONS = ['avi', 'mp4', '3gp', 'mpeg', 'mov', 'flv', 'wmv', 'mkv', 'ts', 'vob', 'm4a', 'mpg'];
const ALLOWED_DOCUMENT_EXTENSIONS = ['txt', 'pdf', 'doc', 'docx', 'rtf', 'xlsx', 'xls'];
const ALLOWED_ARCHIVE_EXTENSIONS = ['zip', 'rar'];

const ALLOWED_EXTENSIONS = [
    ...ALLOWED_AUDIO_EXTENSIONS,
    ...ALLOWED_VIDEO_EXTENSIONS,
    ...ALLOWED_DOCUMENT_EXTENSIONS,
    ...ALLOWED_ARCHIVE_EXTENSIONS,
];

class Question extends React.Component {
  onChangeData = (prop, e) => {
    const state = {};
    state[prop] = e.target.value;
    if (prop === 'questionType') {
      state.questionItems = [];
    }
    return this.onChangeQuestion(state);
  };
  // при изменении вариантов чекбоксов/радио
  onChangeItems = e => {
    const item = e.target.name.split('-')[1];
    const questionItems = [...this.props.questionItems];
    questionItems[item] = e.target.value;

    return this.onChangeQuestion({ questionItems });
  };
  // при изменении значений дэйтпикера
  onDateChange = (value, name) => {
    const item = name.split('-')[1];
    const questionItems = [...this.props.questionItems];
    questionItems[item] = value;

    return this.onChangeQuestion({ questionItems });
  };
  // при изменении значений таймпикера
  onTimeChange = (value, name) => {
    let { questionItems } = this.props;

    const item = name.split('-')[1];
    const items = questionItems.length
      ? [questionItems[0], questionItems[questionItems.length - 1]]
      : [];

    items[item] = value;
    const times = [];

    let time = items[0];

    if (items.length === 2) {
      while (formatDate(time, 'Hmm') <= formatDate(items[1], 'Hmm')) {
        times.push(time);
        time = addMinutes(new Date(time), 30);
      }
    }

    questionItems = times.length ? times : items;
    console.log(times);

    return this.onChangeQuestion({ questionItems });
  };

  onChangeQuestion = state => {
    this.props.onChange({
      ...this.props,
      ...state,
    });
  };

  deleteDocument = idx => {
    let { attachments } = this.props;

    attachments.splice(idx, idx + 1);

    this.onChangeQuestion({ attachments });
  };

  addDocument = file => {
    let { attachments } = this.props;
    attachments = (attachments || []).slice();
    attachments.push(file);
    this.onChangeQuestion({ attachments });
  };

  onFilesUpload = async event => {
    const file = event.target.files[0];
      console.log(file.name);
      const uploadDocument = gql`
      mutation($file: FileUpload!) {
        documents {
          documentsUpload(document: $file) {
            fileUuid
            link
            mimetype
            name
          }
        }
      }
    `;
    await this.props.client
      .mutate({
        mutation: uploadDocument,
        fetchPolicy: 'no-cache',
        variables: { file },
      })
      .then(res => {
        const data = get(res, 'data.documents.documentsUpload');
        if (data) {
          console.log('adding document');
          this.addDocument(data);
        }
      });
  };

  render() {
    const {
      classes,
      id,
      onDelete,
      questionItems,
      title,
      questionType,
      onDataSave,
      attachments,
    } = this.props;
    console.log(attachments);
    return (
      <Card className={classes.card}>
        <CardContent className={classes.cardContent}>
          <div className={classes.header}>
            <TextField
              name="question:title"
              value={title}
              label="Название вопроса"
              className={classes.textField}
              onChange={e => this.onChangeData('title', e)}
              fullWidth
              multiline
              rowsMax={5}
              onBlur={onDataSave}
            />
            <TextField
              name="question:type"
              fullWidth
              select
              label="Тип вопроса"
              className={classes.selectField}
              value={questionType}
              onChange={e => this.onChangeData('questionType', e)}
              onBlur={onDataSave}
            >
              {types.map((option, index) => (
                <MenuItem key={index} value={option.type}>
                  <ListItemIcon>{option.icon}</ListItemIcon>
                  <ListItemText inset primary={option.label} style={{ padding: 0 }} />
                </MenuItem>
              ))}
            </TextField>
          </div>
          {questionType === 'date_range' && (
            <DateRangeQuestion
              items={questionItems}
              onChange={this.onDateChange}
              onDataSave={onDataSave}
            />
          )}
          {questionType === 'checkbox' && (
            <CheckboxQuestion
              items={questionItems}
              namePrefix="questionMultiSelectTypeOption"
              onChange={this.onChangeItems}
              onDataSave={onDataSave}
            />
          )}
          {questionType === 'select' && (
            <CheckboxQuestion
              items={questionItems}
              namePrefix="questionSelectTypeOption"
              onChange={this.onChangeItems}
              onDataSave={onDataSave}
            />
          )}
          {questionType === 'time_range' && (
            <TimeQuestion
              items={questionItems}
              onChange={this.onTimeChange}
              onDataSave={onDataSave}
            />
          )}
        </CardContent>
          {
            attachments &&
            <Images>
              { attachments
                  .filter(img => ALLOWED_PICTURE_EXTENSIONS.includes(img.name.split('.').pop()))
                  .map((img, idx) => (
                <ImageWrapper>
                  <img alt="alt" src={img.link} key={idx} />
                  <RemoveImage
                    variant="fab"
                    color="default"
                    size="mini"
                    onClick={() => this.deleteDocument(idx)}
                  >
                    <Close/>
                  </RemoveImage>
                </ImageWrapper>
              ))}
            </Images>
          }
          <div>
              {
                attachments && attachments
                  .filter(file => ALLOWED_EXTENSIONS.includes(file.name.split('.').pop()))
                  .map((file, idx) => (
                    <FileChip
                      title={file.name}
                    >
                        {/*type icon*/}
                      <FileName onClick={() => this.deleteDocument(idx)}>
                          {file.name}
                      </FileName>
                        <Close/>
                        {/*delete icon*/}
                    </FileChip>
                  ))
              }
          </div>
        <CardActions className={classes.cardActions}>
          <Button component="label" className={classes.button}>
            <UploadIcon />
            <div className={classes.desktopOnly}>прикрепить файл</div>
            <input
              type="file"
              className={classes.fileInput}
              multiple
              onChange={this.onFilesUpload}
            />
          </Button>
          <Button className={classes.button} onClick={() => onDelete(id)}>
            <DeleteIcon />
            <div className={classes.desktopOnly}>Удалить</div>
          </Button>
        </CardActions>
      </Card>
    );
  }
}
Question.defaultProps = {
  questionType: '',
  title: '',
  questionItems: [],
};




export default compose(
  withApollo,
  withStyles(styles)
)(Question);

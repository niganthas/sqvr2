import React from 'react';
import Question from './questions';

const QuestionListEdit = ({ questions, onChange, onDelete, onDataSave }) => (
  <ul>
    {questions.map((question, index) => (
      <li key={question.id}>
        <Question
          onDataSave={onDataSave}
          onChange={onChange}
          onDelete={onDelete}
          index={index}
          {...question}
        />
      </li>
    ))}
  </ul>
);

export default QuestionListEdit;

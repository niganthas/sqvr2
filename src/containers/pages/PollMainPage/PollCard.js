import React from 'react';
import { Avatar } from '@material-ui/core';
import { Text, BaseLink, grid, Button } from 'components';
import styled from 'styled-components';
import { addDays, distanceInWords } from 'date-fns';
import ru from 'date-fns/locale/ru';
import { media, formatDate } from 'utils';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import { compose } from 'recompose';

const UserIcon = styled(Avatar)`
  width: 16px;
  height: 16px;
  margin-right: 8px;
`;

const Header = styled(BaseLink)`
  background: linear-gradient(180deg, #2e87ff 0%, #1060cc 100%);
  box-shadow: 0px 4px 8px rgba(176, 190, 197, 0.24);
  color: #ffffff;
  padding: 12px 16px;
  border-radius: 3px;
  min-height: 64px;
  display: flex;
  flex-direction: column;

  p {
    color: #ffffff;
  }

  ${media.desktop`
    border-radius: 3px 3px 0 0;
    min-height: 174px;
    padding: 20px 24px;
    box-shadow: none;
    
    h3 {
      font-family: 'Fira Sans';
      font-weight: 500;
      line-height: normal;
      font-size: 20px;
      letter-spacing: 0.15px;
    }
  `};
`;

const DescBlock = styled.div`
  display: none;

  ${media.desktop`
    padding: 20px 24px;
    display: flex;
    flex-direction: column;
    flex-grow: 2;
    min-height: 160px;
  `};
`;

const StyledCard = styled.div`
  box-shadow: 0px 4px 8px rgba(176, 190, 197, 0.24);
  border-radius: 3px;
  margin-bottom: 8px;
  width: 100%;

  ${media.desktop`
    width: 49%;
    margin-bottom: 24px;
    display: flex;
    flex-direction: column;
  `};
`;

const PollCard = ({ poll, questions }) => {
  const PollEndingDate = addDays(poll.dateUpdated, 14);
  const PollEndingDays = distanceInWords(PollEndingDate, new Date(), { locale: ru });

  return (
    <StyledCard>
      <Header to={'/polls/' + poll.id}>
        <grid.Flex mb={['4px', '4px', '4px', '14px']}>
          <UserIcon alt={poll.owner.firstName} src={poll.owner.avatarUrl} />
          <Text variant="caption" component="p">
            {poll.owner.firstName} {poll.owner.lastName}
          </Text>
          <Text w="80px" variant="caption" component="p" ml="auto" align="right">
            {formatDate(poll.dateCreated, 'DD.MM.YYYY')}
          </Text>
        </grid.Flex>
        <Text variant="subheading" component="h3" customcolor="#ffffff" mb={[0, 0, 0, '24px']}>
          {poll.title}
        </Text>
        <grid.Desktop mt="auto">
          <Text variant="caption" component="p">
            Завершится через {PollEndingDays}
          </Text>
        </grid.Desktop>
      </Header>

      <DescBlock>
        <Text variant="body2" component="p" customcolor="rgba(0, 0, 0, 0.87)" mb="16px">
          {poll.description}
        </Text>
        {questions &&
          !questions.loading && (
            <Text
              variant="body2"
              component="p"
              customcolor="rgba(0, 0, 0, 0.87)"
              mb="28px"
              mt="auto"
            >
              {questions.polls.allQuestionsOfPoll[0]
                ? '1. ' + questions.polls.allQuestionsOfPoll[0].title
                : ''}
            </Text>
          )}
        <grid.Flex alignItems="center">
          <Button
            to={'/polls/' + poll.id}
            color="primary"
            variant="contained"
            mr="16px"
            style={{ flexShrink: 0 }}
            disabled={poll.isVote}
          >
            {poll.isVote ? 'Ваш голос учтен' : 'Проголосовать'}
          </Button>
          <Text variant="caption" component="p">
            13 из 38 человек проголосовало
          </Text>
        </grid.Flex>
      </DescBlock>
    </StyledCard>
  );
};

export default compose(
  graphql(
    gql`
      query questions($pollId: Int!) {
        polls {
          allQuestionsOfPoll(pollId: $pollId) {
            title
          }
        }
      }
    `,
    {
      name: 'questions',
      options: props => ({ variables: { pollId: props.poll.id } }),
    }
  )
)(PollCard);

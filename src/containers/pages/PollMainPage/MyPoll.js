import React from 'react';
import { Card } from '@material-ui/core';
import { Text } from 'components';
import styled from 'styled-components';
import { baseUrl } from 'utils';
import { withRouter } from 'react-router-dom';

const StyledCard = styled(Card)`
  position: relative;
  box-shadow: 0px 4px 8px rgba(176, 190, 197, 0.24);
  border-radius: 3px;
  margin-bottom: 8px;
  padding: 12px 24px;
  width: 100%;
  cursor: pointer;

  &::before {
    content: '';
    position: absolute;
    width: 8px;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.38);
    border-radius: 3px 0px 0px 3px;
    left: 0;
    top: 0;
  }
`;

const MyPoll = ({ title, id, history }) => {
  return (
    <StyledCard onClick={() => history.push(baseUrl() + '/polls/' + id + '/create-poll')}>
      <Text variant="body1">{title ? title : 'Черновик опроса'}</Text>
    </StyledCard>
  );
};

export default withRouter(MyPoll);

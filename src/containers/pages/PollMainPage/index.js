import React from 'react';
import Assignment from '@material-ui/icons/Assignment';
import {
  Loader,
  Button,
  Text,
  PollsLayout,
  SideBar,
  MainContent,
  SideButton,
  grid,
} from 'components';
import { graphql } from 'react-apollo';
import { compose, withState } from 'recompose';
import gql from 'graphql-tag';
import PollCard from './PollCard';
import MyPoll from './MyPoll';
import ClipBoardIcon from 'components/icons/ClipBoardIcon';
import { withTryCreatePoll } from 'hocs';
import { media } from 'utils';
import { Amplitude } from '@amplitude/react-amplitude';
import styled from 'styled-components';
import { get } from 'lodash';

const NoPollsBlock = styled.div`
  width: 100%;
  padding-top: 30px;

  ${media.desktop`  
    display: flex;
    align-items: flex-end;
    justify-content: center;
    min-height: 300px;
    background: url('/img/no-polls.png') no-repeat top center;
  `};
`;

const Polls = styled.div`
  ${media.desktop`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
  `};
`;

const PollMainPage = props => {
  const { user, allPolls, showAll, setShowAll, createPoll } = props;

  const polls = get(allPolls, 'polls.listPolls') || [];
  const myPoll = polls.find(poll => poll.status === 'draft');
  const activePolls = polls.filter(poll => poll.status !== 'draft');
  const loading = get(allPolls, 'loading');

  const renderMyPoll = myPoll => (
    <grid.Box mb="33px" key={myPoll.id}>
      <Text variant="subheading" component="h2" mb="15px">
        Последний неопубликованный
      </Text>
      <MyPoll key={myPoll.id} title={myPoll.title} id={myPoll.id} />
    </grid.Box>
  );

  const renderPolls = activePolls => (
    <React.Fragment>
      <grid.Flex justifyContent="space-between" mb="15px">
        <Text variant="subheading" component="h2">
          Проведённые опросы
        </Text>
        <Button
          AmplitudeEvent="ToggleAllOrMyPolls"
          p="0"
          size="small"
          color="primary"
          onClick={async () => {
            setShowAll(!showAll);
            await allPolls.refetch();
          }}
        >
          {showAll ? 'Все' : 'Мои'}
        </Button>
      </grid.Flex>
      <Polls>{activePolls.map(poll => <PollCard poll={poll} key={poll.id} />)}</Polls>
    </React.Fragment>
  );

  return (
    <Amplitude
      eventProperties={{
        scope: ['PollsMainPage'],
      }}
    >
      <PollsLayout main title="Опросы" user={user}>
        <SideBar>
          <Text variant="subheading" component="h2" mb="12px">
            Создание опроса
          </Text>
          <SideButton
            variant="contained"
            title="Пустой"
            icon={<ClipBoardIcon />}
            AmplitudeEvent="UserEnterOnCreatePoll"
            onClick={() => createPoll()}
          />
          <SideButton
            variant="contained"
            to="/polls/templates"
            title="Из шаблона"
            icon={<Assignment />}
            AmplitudeEvent="UserEnterOnPollsTemplatesPage"
          />
        </SideBar>
        <grid.Mobile>
          <Text variant="subheading" component="h2" mb="16px">
            Создать опрос
          </Text>
          <grid.Flex justifyContent="space-between">
            <SideButton
              style={{ width: '47%' }}
              variant="contained"
              title="Пустой"
              icon={<ClipBoardIcon />}
              AmplitudeEvent="UserEnterOnCreatePoll"
              onClick={() => createPoll()}
            />
            <SideButton
              style={{ width: '47%' }}
              variant="contained"
              to="/polls/templates"
              title="Из шаблона"
              AmplitudeEvent="UserEnterOnPollsTemplatesPage"
              icon={<Assignment />}
            />
          </grid.Flex>
        </grid.Mobile>
        <MainContent>
          {loading && <Loader loading={loading} />}
          {!polls && (
            <NoPollsBlock>
              <Text variant="title" align="center">
                Еще не создано ни одного опроса
              </Text>
            </NoPollsBlock>
          )}
          {!loading && myPoll && renderMyPoll(myPoll)}
          {!loading && activePolls && renderPolls(activePolls)}
        </MainContent>
      </PollsLayout>
    </Amplitude>
  );
};

const pollsQuery = gql`
  query AllPollsQuery($isAll: Boolean) {
    polls {
      listPolls(isAll: $isAll) {
        id
        title
        description
        dateCreated
        dateUpdated
        status
        isVote
        owner {
          avatarUrl
          firstName
          lastName
        }
      }
    }
  }
`;

export default compose(
  withState('showAll', 'setShowAll', false),
  withTryCreatePoll,
  graphql(pollsQuery, {
    name: 'allPolls',
    options: props => ({
      fetchPolicy: 'network-only',
      variables: { isAll: props.showAll },
    }),
  })
)(PollMainPage);

import React from 'react';
import { Button, Modal, Text, grid } from 'components';
import { withStyles, DialogActions, DialogContent, DialogTitle, Icon } from '@material-ui/core';
import CheckIcon from '@material-ui/icons/CheckCircle';
import { baseUrl } from 'utils';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';

const styles = {
  title: {
    color: '#00DA71',
  },
  icon: {
    color: '#00DA71',
  },
};

const ModalSuccess = props => {
  const { classes, open, close, history, match } = props;
  return (
    <Modal open={open} onClose={close}>
      <DialogTitle>
        <grid.Flex alignItems="center">
          <Icon style={{ marginRight: 8 }}>
            <CheckIcon className={classes.icon} />
          </Icon>
          <Text variant="title" component="span" className={classes.title}>
            Ваш голос учтён!
          </Text>
        </grid.Flex>
      </DialogTitle>
      <DialogContent>
        <Text variant="body2" className={classes.desc} mb="32px">
          Теперь вы можете ознакомиться с результатами
        </Text>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={() => {
            close();
            history.push(baseUrl() + '/polls/main');
          }}
        >
          все опросы
        </Button>
        <Button
          onClick={() => {
            close();
            history.push(baseUrl() + '/polls/' + match.params.id + '/results');
          }}
          color="primary"
        >
          результаты
        </Button>
      </DialogActions>
    </Modal>
  );
};

export default compose(
  withStyles(styles),
  withRouter
)(ModalSuccess);

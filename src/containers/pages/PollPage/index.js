import React from 'react';
import { IconButton } from '@material-ui/core';
import SendIcon from '@material-ui/icons/Send';
import {
  PollsLayout,
  Text,
  Button,
  SideBar,
  MainContent,
  SideButton,
  Loader,
  PollUserBlock,
} from 'components';
import DeleteIcon from '@material-ui/icons/Delete';
import SignalAltIcon from 'components/icons/SignalAltIcon';
import Modal from './Modal';
import ModalSuccess from './ModalSuccess';
import { withState, compose } from 'recompose';
import { graphql } from 'react-apollo';
import { Redirect } from 'react-router-dom';
import gql from 'graphql-tag';
import { get } from 'lodash';
import { baseUrl } from 'utils';
import Question from './questions';
import { Amplitude } from '@amplitude/react-amplitude';

const PollPage = ({
  match,
  state,
  setState,
  user,
  allPolls,
  vote,
  history,
  setArchiveStatus,
  questionsValues,
  setQuestionsValues,
}) => {
  const listPolls = get(allPolls, 'polls.listPolls') || [];
  const allQuestionsOfPoll = get(allPolls, 'polls.allQuestionsOfPoll') || [];
  const poll = listPolls.find(
    poll => poll.id === parseInt(match.params.id, 10) && poll.status === 'active'
  );
  const loading = get(allPolls, 'loading');

  const getValue = (vote, pollQuestionId) => {
    const value = {
      pollQuestionId,
      vote,
    };
    setQuestionsValues([
      ...questionsValues.filter(q => q.pollQuestionId !== value.pollQuestionId),
      value,
    ]);
  };

  const sendResults = async () => {
    if (poll.isVote) {
      alert('Ваш голос уже был учтен');
      return;
    }
    if (questionsValues.length < allQuestionsOfPoll.length) {
      alert('Не на все вопросы выбраны ответы');
      return;
    }
    await vote({
      variables: {
        pollId: poll.id,
        questions: questionsValues,
      },
    });
    setState({ success: true });
  };

  if (loading) {
    return <Loader loading={loading} />;
  }

  return (
    <Amplitude>
      <PollsLayout
        title="Голосование"
        icon={
          <IconButton onClick={sendResults}>
            <SendIcon />
          </IconButton>
        }
        user={user}
        onBackButtonClick={() => {
          history.push(`${baseUrl()}/polls/main`);
        }}
      >
        {!poll && <Text variant="title">Такого опроса не существует</Text>}

        {poll &&
          poll.owner.id !== user.id &&
          poll.isVote && <Redirect to={`${baseUrl()}/polls/${match.params.id}/results`} />}

        {poll && (
          <React.Fragment>
            <SideBar>
              <Text variant="subheading" component="h2" mb="12px">
                Голосование
              </Text>
              <SideButton
                AmplitudeEvent="UserSharedThePollLink"
                variant="contained"
                title="поделиться"
                icon={<SendIcon />}
                onClick={() => setState({ open: !state.open })}
              />
              {user.id === poll.owner.id ? (
                <React.Fragment>
                  <SideButton
                    AmplitudeEvent="UserEnterOnResultsFromPoll"
                    variant="contained"
                    title="посмотреть результаты"
                    to={'/polls/' + match.params.id + '/results'}
                    icon={<SignalAltIcon />}
                  />
                  <SideButton
                    AmplitudeEvent="UserArchivedPoll"
                    variant="contained"
                    title="отменить опрос"
                    icon={<DeleteIcon />}
                    onClick={async () => {
                      await setArchiveStatus();
                      alert('Опрос перемещен в архив');
                    }}
                  />
                </React.Fragment>
              ) : (
                ''
              )}
            </SideBar>
            <MainContent>
              <PollUserBlock
                src={poll.owner.avatarUrl}
                name={`${poll.owner.firstName} ${poll.owner.lastName}`}
                date={poll.dateUpdated}
              />
              <Text variant="title" mb="8px">
                {poll.title}
              </Text>
              <Text mb="25px" component="p" variant="caption">
                {poll.description}
              </Text>
              {allQuestionsOfPoll.map((question, index) => (
                <Question
                  key={index}
                  type={question.questionType}
                  index={index}
                  {...question}
                  getValue={getValue}
                  id={question.id}
                />
              ))}
              <Button
                my="30px"
                variant="contained"
                color="primary"
                AmplitudeEvent="UserVotedInThePoll"
                onClick={sendResults}
                disabled={poll.isVote}
                w="272px"
              >
                {poll.isVote ? 'ваш голос уже учтен' : 'проголосовать'}
              </Button>
            </MainContent>
            <Modal
              open={!!state.open}
              close={() => {
                setState({ open: false });
              }}
            />
            <ModalSuccess
              open={state.success}
              close={() => {
                setState({ success: false });
              }}
            />
          </React.Fragment>
        )}
      </PollsLayout>
    </Amplitude>
  );
};

const pollsQuery = gql`
  query AllPollsQuery($isAll: Boolean, $pollId: Int!) {
    polls {
      listPolls(isAll: $isAll) {
        id
        isVote
        dateUpdated
        title
        description
        status
        owner {
          id
          firstName
          lastName
          avatarUrl
        }
      }
      allQuestionsOfPoll(pollId: $pollId) {
        id
        title
        questionType
        questionItems
      }
    }
  }
`;

const voteMutation = gql`
  mutation VoteMutation($pollId: Int!, $questions: [QuestionVote]!) {
    polls {
      vote(pollId: $pollId, questions: $questions) {
        questions {
          pollQuestionId
        }
      }
    }
  }
`;

export default compose(
  withState('questionsValues', 'setQuestionsValues', []),
  withState('state', 'setState', { open: false, success: false, votes: [] }),
  graphql(pollsQuery, {
    name: 'allPolls',
    options: props => ({ variables: { isAll: true, pollId: props.match.params.id } }),
  }),
  graphql(voteMutation, {
    name: 'vote',
    options: props => ({ variables: { pollId: props.match.params.id } }),
  }),
  graphql(
    gql`
      mutation SetStatus($status: String!, $pollId: Int!) {
        polls {
          setStatusPoll(status: $status, pollId: $pollId) {
            status
          }
        }
      }
    `,
    {
      name: 'setArchiveStatus',
      options: props => ({ variables: { status: 'archived', pollId: props.match.params.id } }),
    }
  )
)(PollPage);

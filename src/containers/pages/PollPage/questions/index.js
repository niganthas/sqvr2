import React from 'react';
import MultipleDateQuestion from './MultipleDateQuestion';
import IntegerRangeQuestion from './IntegerRangeQuestion';
import TimeQuestion from './TimeQuestion';
import SelectQuestion from './SelectQuestion';
import CheckboxQuestion from './CheckboxQuestion';
import { Text, grid } from 'components';

const QUESTION_COMPONENTS = {
  date_range: MultipleDateQuestion,
  integer_range: IntegerRangeQuestion,
  time_range: TimeQuestion,
  select: SelectQuestion,
  checkbox: CheckboxQuestion,
};

const Question = ({ type, index, id, onChange, getValue, ...props }) => {
  const QuestionComponent = QUESTION_COMPONENTS[type];

  return (
    <grid.Box mb="40px">
      <Text variant="subheading" mb="15px">
        {index + 1}
        {'. '}
        {props.title}
      </Text>
      <QuestionComponent
        onValueChange={value => getValue(value, id)}
        options={props.questionItems}
        {...props}
      />
    </grid.Box>
  );
};

export default Question;

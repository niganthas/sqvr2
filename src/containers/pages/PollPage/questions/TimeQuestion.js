import React from 'react';
import { FormControlLabel, FormGroup, Checkbox } from '@material-ui/core';
import { grid } from 'components';
import styled from 'styled-components';
import { withState } from 'recompose';
import { formatDate } from 'utils';

const Label = styled(FormControlLabel)`
  background-color: rgba(0, 0, 0, 0.04);
  margin-right: 0;
  border-radius: 3px;
  margin-bottom: 8px;
`;

const TimeQuestion = ({ options, onValueChange, setValue, value }) => (
  <grid.Box pl="10px">
    <FormGroup>
      {options.map((option, index) => (
        <Label
          key={index}
          value={option}
          control={
            <Checkbox
              value={options[0]}
              onChange={e => {
                e.target.checked
                  ? value.push(index)
                  : (value = value.filter(item => item !== index));
                setValue(value);
                onValueChange(value);
              }}
              color="primary"
            />
          }
          label={formatDate(option, 'HH:mm')}
        />
      ))}
    </FormGroup>
  </grid.Box>
);

export default withState('value', 'setValue', [])(TimeQuestion);

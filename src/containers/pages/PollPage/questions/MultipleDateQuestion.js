import React from 'react';
import {
  withMultipleDates,
  defaultMultipleDateInterpolation,
  Calendar,
} from 'react-infinite-calendar';
import { CalendarTemplate } from 'components';
import { withState } from 'recompose';
import { formatDate } from 'utils';

const MultipleDateQuestion = ({ questionItems, dates, setDates, onValueChange }) => (
  <CalendarTemplate
    Component={withMultipleDates(Calendar)}
    selected={dates}
    interpolateSelection={defaultMultipleDateInterpolation}
    min={new Date(questionItems[0])}
    max={new Date(questionItems[1])}
    minDate={new Date(questionItems[0])}
    maxDate={new Date(questionItems[1])}
    onSelect={value => {
      let newDates;
      if (dates.every(date => formatDate(value, 'DD MM') !== formatDate(date, 'DD MM'))) {
        newDates = [...dates, value];
      } else {
        newDates = dates.filter(date => formatDate(date, 'DD MM') !== formatDate(value, 'DD MM'));
      }
      setDates(newDates);
      onValueChange(newDates);
      console.log(newDates);
    }}
  />
);

export default withState('dates', 'setDates', p => [new Date(p.questionItems[0])])(
  MultipleDateQuestion
);

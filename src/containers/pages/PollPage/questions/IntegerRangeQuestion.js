import React from 'react';
import { withState } from 'recompose';
import { RangeSlider, grid } from 'components';
import { FormControlLabel, Radio, RadioGroup } from '@material-ui/core';
import styled from 'styled-components';

const StyledGroup = styled(RadioGroup)`
  display: flex;
  flex-direction: row;
`;
const Label = styled(FormControlLabel)`
  display: flex;
  flex-direction: column-reverse;
`;

const rating = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];

const IntegerRangeQuestion = ({ ratingValue, setRatingValue, onValueChange }) => (
  <React.Fragment>
    <grid.Mobile>
      <RangeSlider
        handleLabel={ratingValue}
        min={1}
        max={10}
        value={+ratingValue}
        onChange={value => {
          setRatingValue(value.toString());
          onValueChange(value);
        }}
      />
    </grid.Mobile>
    <grid.Desktop>
      <StyledGroup
        value={ratingValue}
        name="rating"
        onChange={e => {
          setRatingValue(e.target.value);
          onValueChange(e.target.value, e);
        }}
      >
        {rating.map((rate, index) => (
          <Label key={index} value={rate} control={<Radio color="primary" />} label={rate} />
        ))}
      </StyledGroup>
    </grid.Desktop>
  </React.Fragment>
);

export default withState('ratingValue', 'setRatingValue', '5')(IntegerRangeQuestion);

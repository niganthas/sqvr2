import React from 'react';
import Helmet from 'react-helmet';
import { MuiThemeProvider } from '@material-ui/core/styles';
import theme from 'themes/default-sqvr';
import 'react-infinite-calendar/styles.css';
import './styles.scss';

const App = ({ children }) => {
  return (
    <React.Fragment>
      <Helmet
        htmlAttributes={{ lang: 'ru', amp: undefined }} // amp takes no value
        titleTemplate="Система Сквер - %s"
        defaultTitle="Система Сквер 2"
        link={[
          { rel: 'apple-touch-icon', sizes: '180x180', href: '/images/icons/apple-touch-icon.png' },
          {
            rel: 'icon',
            type: 'image/png',
            href: '/images/icons/favicon-32x32.png',
            sizes: '32x32',
          },
          {
            rel: 'icon',
            type: 'image/png',
            href: '/images/icons/favicon-16x16.png',
            sizes: '16x16',
          },
          { rel: 'manifest', href: '/images/icons/manifest.json' },
          { rel: 'mask-icon', href: '/images/icons/safari-pinned-tab.svg', color: '#2c83b5' },
          { rel: 'shortcut icon', href: '/favicon.ico?v=214' },
        ]}
        meta={[
          { charset: 'utf-8' },
          { name: 'viewport', content: 'width=device-width, initial-scale=1.0' },
          { name: 'format-detection', content: 'telephone=no' },
          { name: 'keywords', content: 'sqvr, front' },
          { name: 'description', content: 'Система Сквер' },
          { property: 'og:title', content: 'Система Сквер' },
          { property: 'og:site_name', content: 'Система Сквер' },
          { property: 'og:description', content: 'Система Сквер' },
          { name: 'apple-mobile-web-app-title', content: 'Система Сквер' },
          { name: 'application-name', content: 'Система Сквер' },
          { name: 'msapplication-TileColor', content: '#2b5797' },
          { name: 'msapplication-TileImage', content: '/images/icons/mstile-150x150.png' },
          { name: 'msapplication-config', content: '/images/icons/browserconfig.xml' },
          { name: 'theme-color', content: '#ffffff' },
        ]}
      />
      <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>
    </React.Fragment>
  );
};

export default App;

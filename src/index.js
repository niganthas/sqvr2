import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router';
import { AppContainer } from 'react-hot-loader';
import createBrowserHistory from 'history/createBrowserHistory';
import { Switch } from 'react-router-dom';
import { ApolloProvider } from 'react-apollo';
import JssProvider from 'react-jss/lib/JssProvider';
import { create } from 'jss';
import { createGenerateClassName, jssPreset } from '@material-ui/core/styles';

import { baseUrl } from 'utils';
import App from './containers/layouts/App';
import routeComponents from 'routes';
import { client } from './apollo';

import amplitude from 'amplitude-js';
import { AmplitudeProvider } from '@amplitude/react-amplitude';
const AMPLITUDE_KEY = '3276e735ea5b6dbfd77371e622df8bfa';

const generateClassName = createGenerateClassName();
const jss = create(jssPreset());
// We define a custom insertion point that JSS will look for injecting the styles in the DOM.
jss.options.insertionPoint = document.getElementById('jss-insertion-point');

const appRoot = document.getElementById('root');
const history = createBrowserHistory();

const render = AppComponent => {
  ReactDOM.render(
    <JssProvider jss={jss} generateClassName={generateClassName}>
      <AmplitudeProvider amplitudeInstance={amplitude.getInstance()} apiKey={AMPLITUDE_KEY}>
        <Router key={Math.random()} basename={baseUrl()} history={history}>
          <ApolloProvider client={client}>
            <AppContainer>
              <AppComponent>
                <Switch>{routeComponents}</Switch>
              </AppComponent>
            </AppContainer>
          </ApolloProvider>
        </Router>
      </AmplitudeProvider>
    </JssProvider>,
    appRoot
  );
};

render(App);

// enable hot reloading, will be stripped in production
if (process.env.NODE_ENV !== 'production' && module.hot) {
  module.hot.accept(() => {
    render(App);
  });
}
